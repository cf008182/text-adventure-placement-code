/*
 *	ScottFree Revision 1.14
 *
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 *
 *
 *	You must have an ANSI C compiler to build this program.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
// Curses and signals includes removed. - MML
// Time include added. - MML
#include <time.h>

#include "Scott.h"

// Configuration for weird platforms removed. - MML

Header GameHeader;
Tail GameTail;
Item *Items;
Room *Rooms;
char **Verbs;
char **Nouns;
char **Messages;
Action *Actions;
int LightRefill;
char NounText[16];
int Counters[16];	/* Range unknown */
int CurrentCounter;
int SavedRoom;
int RoomSaved[16];	/* Range unknown */
int Redraw;		/* Update item window */
int Options;		/* Option flags set */
int Width;		/* Terminal width */
// Curses variables removed. - MML

int randstate = 0;	// State for pseudo-random number generator. - MML

#define TRS80_LINE	"\n<------------------------------------------------------------>\n"

#define MyLoc	(GameHeader.PlayerRoom)

long BitFlags=0;	/* Might be >32 flags - I haven't seen >32 yet */

void Fatal(char *x)
{
	// Curses code removed. - MML
	fprintf(stderr,"%s.\n",x);
	exit(1);
}

void *MemAlloc(int size)
{
	void *t=(void *)malloc(size);
	if(t==NULL)
		Fatal("Out of memory");
	return(t);
}

char *ReadString(FILE *f)
{
	char tmp[1024];
	char *t;
	int c,nc;
	int ct=0;
oops:	do
	{
		c=fgetc(f);
	}
	while(c!=EOF && isspace(c));
	if(c!='"')
	{
		Fatal("Initial quote expected");
	}
	do
	{
		c=fgetc(f);
		if(c==EOF)
			Fatal("EOF in string");
		if(c=='"')
		{
			nc=fgetc(f);
			if(nc!='"')
			{
				ungetc(nc,f);
				break;
			}
		}
		if(c==0x60) 
			c='"'; /* pdd */
		tmp[ct++]=c;
	}
	while(1);
	tmp[ct]=0;
	t=MemAlloc(ct+1);
	memcpy(t,tmp,ct+1);
	return(t);
}
	
void LoadDatabase(FILE *f, int loud)
{
	int ni,na,nw,nr,mc,pr,tr,wl,lt,mn,trm;
	int ct;
	short lo;
	Action *ap;
	Room *rp;
	Item *ip;
/* Load the header */
	
	if(fscanf(f,"%*d %d %d %d %d %d %d %d %d %d %d %d",
		&ni,&na,&nw,&nr,&mc,&pr,&tr,&wl,&lt,&mn,&trm,&ct)<10)
		Fatal("Invalid database(bad header)");
	GameHeader.NumItems=ni;
	Items=(Item *)MemAlloc(sizeof(Item)*(ni+1));
	GameHeader.NumActions=na;
	Actions=(Action *)MemAlloc(sizeof(Action)*(na+1));
	GameHeader.NumWords=nw;
	GameHeader.WordLength=wl;
	Verbs=(char **)MemAlloc(sizeof(char *)*(nw+1));
	Nouns=(char **)MemAlloc(sizeof(char *)*(nw+1));
	GameHeader.NumRooms=nr;
	Rooms=(Room *)MemAlloc(sizeof(Room)*(nr+1));
	GameHeader.MaxCarry=mc;
	GameHeader.PlayerRoom=pr;
	GameHeader.Treasures=tr;
	GameHeader.LightTime=lt;
	LightRefill=lt;
	GameHeader.NumMessages=mn;
	Messages=(char **)MemAlloc(sizeof(char *)*(mn+1));
	GameHeader.TreasureRoom=trm;
	
/* Load the actions */

	ct=0;
	ap=Actions;
	if(loud)
		printf("Reading %d actions.\n",na);
	while(ct<na+1)
	{
		if(fscanf(f,"%hd %hd %hd %hd %hd %hd %hd %hd",
			&ap->Vocab,
			&ap->Condition[0],
			&ap->Condition[1],
			&ap->Condition[2],
			&ap->Condition[3],
			&ap->Condition[4],
			&ap->Action[0],
			&ap->Action[1])!=8)
		{
			printf("Bad action line (%d)\n",ct);
			exit(1);
		}
		ap++;
		ct++;
	}			
	ct=0;
	if(loud)
		printf("Reading %d word pairs.\n",nw);
	while(ct<nw+1)
	{
		Verbs[ct]=ReadString(f);
		Nouns[ct]=ReadString(f);
		ct++;
	}
	ct=0;
	rp=Rooms;
	if(loud)
		printf("Reading %d rooms.\n",nr);
	while(ct<nr+1)
	{
		fscanf(f,"%hd %hd %hd %hd %hd %hd",
			&rp->Exits[0],&rp->Exits[1],&rp->Exits[2],
			&rp->Exits[3],&rp->Exits[4],&rp->Exits[5]);
		rp->Text=ReadString(f);
		ct++;
		rp++;
	}
	ct=0;
	if(loud)
		printf("Reading %d messages.\n",mn);
	while(ct<mn+1)
	{
		Messages[ct]=ReadString(f);
		ct++;
	}
	ct=0;
	if(loud)
		printf("Reading %d items.\n",ni);
	ip=Items;
	while(ct<ni+1)
	{
		ip->Text=ReadString(f);
		ip->AutoGet=strchr(ip->Text,'/');
		/* Some games use // to mean no auto get/drop word! */
		if(ip->AutoGet && strcmp(ip->AutoGet,"//") && strcmp(ip->AutoGet,"/*"))
		{
			char *t;
			*ip->AutoGet++=0;
			t=strchr(ip->AutoGet,'/');
			if(t!=NULL)
				*t=0;
		}
		fscanf(f,"%hd",&lo);
		ip->Location=(unsigned char)lo;
		ip->InitialLoc=ip->Location;
		ip++;
		ct++;
	}
	ct=0;
	/* Discard Comment Strings */
	while(ct<na+1)
	{
		free(ReadString(f));
		ct++;
	}
	fscanf(f,"%d",&ct);
	if(loud)
		printf("Version %d.%02d of Adventure ",
		ct/100,ct%100);
	fscanf(f,"%d",&ct);
	if(loud)
		printf("%d.\nLoad Complete.\n\n",ct);
}

// Dump string s to FILE f, escaping according to C literal rules. - MML
void DumpString(FILE *f, char* s) {
	if (s == NULL) {
		fprintf(f, "NULL");
		return;
	}
	fputc('"', f);
	while (*s != '\0') {
		switch (*s) {
			case '"':
				fprintf(f, "\\\"");
				break;
			case '\n':
				fprintf(f, "\\n");
				break;
			case '\\':
				fprintf(f, "\\\\");
				break;
			default:
				fputc(*s, f);
				break;
		
		}
		s++;
	}
	fputc('"', f);
}

// This method dumps the contents of the database as a C header file. - MML
// This can be included in the modified ScottFree interpreter to fix the game.
// This has several uses:
// 1. If you wanted to use some existing C verification/analysis tool on the
// interpreter/game combination, it makes the whole thing self-contained.
// 2. It makes executing moves faster, as they can be optimised better by the
// compiler.
// 3. It provides data structures that can be easily examined by the search
// tool to work out what the valid verbs/nouns are, how many rooms/messages
// there are, and so on.
void DumpDatabase(FILE *f, int loud)
{
	int m,n = 0;

	// Add an extra message to encode winning the game if treasures are used.
	int has_treasure = GameHeader.Treasures > 0 ? 1 : 0;

	fprintf(f, "#define MAX_ITEM %d\n", GameHeader.NumItems);
	fprintf(f, "#define MAX_ACTION %d\n", GameHeader.NumActions);
	fprintf(f, "#define MAX_WORD %d\n", GameHeader.NumWords);
	fprintf(f, "#define MAX_ROOM %d\n", GameHeader.NumRooms);
	fprintf(f, "#define MAX_CARRY %d\n", GameHeader.MaxCarry);
	fprintf(f, "#define PLAYER_START %d\n", GameHeader.PlayerRoom);
	fprintf(f, "#define TREASURES %d\n", GameHeader.Treasures);
	fprintf(f, "#define WORD_LENGTH %d\n", GameHeader.WordLength);
	fprintf(f, "#define MAX_MESSAGE %d\n", GameHeader.NumMessages + has_treasure);
	fprintf(f, "#define TREASURE_ROOM %d\n", GameHeader.TreasureRoom);
	// Some games have light refills of 10,000 or more. Surely meant to be infinite?
	fprintf(f, "#define LIGHT_REFILL %d\n", LightRefill > 255 ? 255 : LightRefill);
	fprintf(f, "\n");

	fprintf(f, "typedef struct {\n");
	fprintf(f, "\tuint32_t BitFlags;\n");
	fprintf(f, "\tuint8_t GameOver;\n");
	fprintf(f, "\tuint8_t PlayerRoom;\n");
	fprintf(f, "\tuint8_t LightTime;\n");
	fprintf(f, "\tuint8_t RandState;\n");
	fprintf(f, "\tint8_t CurrentCounter;\n");
	fprintf(f, "\tuint8_t SavedRoom;\n");
	fprintf(f, "\tint8_t Counters[16];\n");
	fprintf(f, "\tuint8_t RoomSaved[16];\n");
	fprintf(f, "\tuint8_t ItemLocs[MAX_ITEM + 1];\n");
	fprintf(f, "} GameState;\n");

	fprintf(f, "#ifdef INTERPRETER\n");

	fprintf(f, "Item Items[] = {\n");
	for (n = 0; n <= GameHeader.NumItems; n++) {
		fprintf(f, "    {\n");
		fprintf(f, "        ");
		DumpString(f, Items[n].Text);
		fprintf(f, ", // Text\n");
		fprintf(f, "        %d, // InitialLoc\n", Items[n].InitialLoc);
		fprintf(f, "        ");
		DumpString(f, Items[n].AutoGet);
		fprintf(f, ", // AutoGet\n");
		fprintf(f, "    },\n");
	}
	fprintf(f, "};\n");

	fprintf(f, "char* Verbs[] = {\n");
	for (n = 0; n <= GameHeader.NumWords; n++) {
		fprintf(f, "    ");
		DumpString(f, Verbs[n]);
		fprintf(f, ",\n");
	}
	fprintf(f, "};\n");

	fprintf(f, "char* Nouns[] = {\n");
	for (n = 0; n <= GameHeader.NumWords; n++) {
		fprintf(f, "    ");
		DumpString(f, Nouns[n]);
		fprintf(f, ",\n");
	}
	fprintf(f, "};\n");
	
	fprintf(f, "Room Rooms[] = {\n");
	for (n = 0; n <= GameHeader.NumRooms; n++) {
		fprintf(f, "    {\n");
		fprintf(f, "        ");
		DumpString(f, Rooms[n].Text);
		fprintf(f, ", // Text\n");
		fprintf(f, "        {");
		for (m = 0; m < 6; m++) {
			fprintf(f, "%d, ", Rooms[n].Exits[m]);
		}
		fprintf(f, "}\n");
		fprintf(f, "    },\n");
	}
	fprintf(f, "};\n");

	fprintf(f, "Action Actions[] = {\n");
	for (n = 0; n <= GameHeader.NumActions; n++) {
		fprintf(f, "    {%d, {%d, %d, %d, %d, %d}, {%d, %d}},\n",
			Actions[n].Vocab,
			Actions[n].Condition[0], Actions[n].Condition[1],
			Actions[n].Condition[2], Actions[n].Condition[3],
			Actions[n].Condition[4],
			Actions[n].Action[0], Actions[n].Action[1]
		);
	}
	fprintf(f, "};\n");

	fprintf(f, "char* Messages[] = {\n");
	for (n = 0; n <= GameHeader.NumMessages; n++) {
		fprintf(f, "    ");
		DumpString(f, Messages[n]);
		fprintf(f, ",\n");
	}
	if (has_treasure) {
		fprintf(f, "    \"All treasures collected.\",\n");
	}
	fprintf(f, "};\n");


	fprintf(f, "const GameState InitState = {\n");
	fprintf(f, "\t0, 0, PLAYER_START, LIGHT_REFILL, 0, 0, 0, {0}, {0},\n");
	fprintf(f, "\t{\n");
	for (n = 0; n <= GameHeader.NumItems; n++) {
		fprintf(f, "\t\t%d,\n", Items[n].InitialLoc);
	}
	fprintf(f, "\t}\n");
	fprintf(f, "};\n");
	
	fprintf(f, "#else\n");

	fprintf(f, "extern Item Items[];\n");
	fprintf(f, "extern char* Verbs[];\n");
	fprintf(f, "extern char* Nouns[];\n");
	fprintf(f, "extern Room Rooms[];\n");
	fprintf(f, "extern Action Actions[];\n");
	fprintf(f, "extern char* Messages[];\n");
	fprintf(f, "extern GameState InitState;\n");
	
	fprintf(f, "#endif\n");

}


int OutputPos=0;

void OutReset()
{
	OutputPos=0;
	// Curses code removed. - MML
}

void OutBuf(char *buffer)
{
	char word[80];
	int wp;
	while(*buffer)
	{
		if(OutputPos==0)
		{
			while(*buffer && isspace(*buffer))
			{
				if(*buffer=='\n')
				{
					// Curses code removed. - MML
					printf("\n");
					OutputPos=0;
				}
				buffer++;
			}
		}
		if(*buffer==0)
			return;
		wp=0;
		while(*buffer && !isspace(*buffer))
		{
			word[wp++]=*buffer++;
		}
		word[wp]=0;
/*		fprintf(stderr,"Word '%s' at %d\n",word,OutputPos);*/
		if(OutputPos+strlen(word)>(Width-2))
		{
			// Curses code removed. - MML
			printf("\n");
			OutputPos=0;
		}
		// Curses code removed. - MML
		fputs(word, stdout);
		OutputPos+=strlen(word);
		
		if(*buffer==0)
			return;
		
		if(*buffer=='\n')
		{
			// Curses code removed. - MML
			printf("\n");
			OutputPos=0;
		}
		else
		{
			OutputPos++;
			if(OutputPos<(Width-1)) {
				// Curses code removed. - MML
				printf(" ");
			}
		}
		buffer++;
	}
}

void Output(char *a)
{
	char block[512];
	strcpy(block,a);
	OutBuf(block);
}

void OutputNumber(int a)
{
	char buf[16];
	sprintf(buf,"%d ",a);
	OutBuf(buf);
}
		
void main(int argc, char *argv[])
{
	FILE *f;
	int vb,no;
	
	while(argv[1])
	{
		if(*argv[1]!='-')
			break;
		switch(argv[1][1])
		{
			case 'd':
				Options|=DEBUGGING;
				break;
			case 'h':
			default:
				fprintf(stderr,"%s: [-d] <gamename> <database.h>.\n",
						argv[0]);
				exit(1);
		}
		if(argv[1][2]!=0)
		{
			fprintf(stderr,"%s: option -%c does not take a parameter.\n",
				argv[0],argv[1][1]);
			exit(1);
		}
		argv++;
		argc--;
	}			

	if(argc!=3)
	{
		fprintf(stderr,"%s <database> <database.h>.\n",argv[0]);
		exit(1);
	}
	f=fopen(argv[1],"r");
	if(f==NULL)
	{
		perror(argv[1]);
		exit(1);
	}

	OutReset();
	Width=80;
	OutBuf("\
Scott Free, A Scott Adams game driver in C.\n\
Release 1.14, (c) 1993,1994,1995 Swansea University Computer Society.\n\
Distributed under the GNU software license\n\n");
	LoadDatabase(f,(Options&DEBUGGING)?1:0);
	fclose(f);

	// Dump the database as a suitable C header file. - MML
	f=fopen(argv[2],"w");
	if(f==NULL)
	{
		perror(argv[2]);
		exit(1);
	}
	DumpDatabase(f,(Options&DEBUGGING)?1:0);
	fclose(f);
}
