#define MAX_ITEM 6
#define MAX_ACTION 6
#define MAX_WORD 18
#define MAX_ROOM 5
#define MAX_CARRY 6
#define PLAYER_START 3
#define TREASURES 1
#define WORD_LENGTH 3
#define MAX_MESSAGE 5
#define TREASURE_ROOM 1
#define LIGHT_REFILL 125

typedef struct {
	uint32_t BitFlags;
	uint8_t GameOver;
	uint8_t PlayerRoom;
	uint8_t LightTime;
	uint8_t RandState;
	int8_t CurrentCounter;
	uint8_t SavedRoom;
	int8_t Counters[16];
	uint8_t RoomSaved[16];
	uint8_t ItemLocs[MAX_ITEM + 1];
} GameState;
#ifdef INTERPRETER
Item Items[] = {
    {
        "Sign says: leave treasure here, then say SCORE", // Text
        1, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Wooden cross", // Text
        2, // InitialLoc
        "CROSS", // AutoGet
    },
    {
        "Locked door", // Text
        3, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Brass key", // Text
        5, // InitialLoc
        "KEY", // AutoGet
    },
    {
        "Open door leads south", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "*Gold coin*", // Text
        4, // InitialLoc
        "COIN", // AutoGet
    },
    {
        "Vampire", // Text
        5, // InitialLoc
        NULL, // AutoGet
    },
};
char* Verbs[] = {
    "<auto>",
    "GO",
    "OPEN",
    "SCORE",
    "INVENTORY",
    "",
    "",
    "",
    "",
    "",
    "GET",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "DROP",
};
char* Nouns[] = {
    "<any>",
    "NORTH",
    "SOUTH",
    "EAST",
    "WEST",
    "UP",
    "DOWN",
    "KEY",
    "COIN",
    "CROSS",
    "DOOR",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
};
Room Rooms[] = {
    {
        "[nowhere]", // Text
        {0, 0, 0, 0, 0, 0, }
    },
    {
        "gorgeously decorated throne room", // Text
        {0, 2, 0, 0, 0, 0, }
    },
    {
        "square chamber", // Text
        {1, 0, 3, 0, 0, 0, }
    },
    {
        "gloomy dungeon", // Text
        {5, 0, 0, 2, 0, 0, }
    },
    {
        "dungeon cell", // Text
        {3, 0, 0, 0, 0, 0, }
    },
    {
        "damp, dismal crypt", // Text
        {0, 3, 0, 0, 0, 0, }
    },
};
Action Actions[] = {
    {450, {0, 0, 0, 0, 0}, {9750, 0}},
    {600, {0, 0, 0, 0, 0}, {9900, 0}},
    {310, {42, 72, 0, 0, 0}, {150, 0}},
    {310, {42, 40, 80, 0, 0}, {10802, 0}},
    {160, {82, 80, 0, 0, 0}, {8100, 0}},
    {100, {122, 26, 0, 0, 0}, {513, 0}},
    {100, {122, 21, 0, 0, 0}, {600, 0}},
};
char* Messages[] = {
    "[dummy]",
    "It's locked.",
    "OK",
    "Vampire bites me!  I'm dead!",
    "Vampire cowers away from the cross!",
    "All treasures collected.",
};
GameState InitState = {
	0, 0, PLAYER_START, LIGHT_REFILL, 0, 0, 0, {0}, {0},
	{
		1,
		2,
		3,
		5,
		0,
		4,
		5,
	}
};
#else
extern Item Items[];
extern char* Verbs[];
extern char* Nouns[];
extern Room Rooms[];
extern Action Actions[];
extern char* Messages[];
extern GameState InitState;
#endif
