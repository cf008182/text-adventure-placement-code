#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo 'Example: ./scamper.sh games/saga/adv01.dat'
    exit 0
fi

echo Building dumper.
cd dumper && make && cd ..
base=$(basename "$1" .dat)
echo Adventure file: $1

[ -d "$base" ] && echo "Directory $base already exists. Delete before proceeding." && exit 1

echo Creating output directory: $base
cp -r scamper $base
echo Dumping database: $base/db.h
./dumper/dumper "$1" "$base/db.h"
echo Compiling ScAmPER.
cd "$base" && make && cd ..
echo Running ScAmPER.
cd "$base" && (./scamper | tee log.txt) && cd ..
echo Building visualisation.
cd "$base/maps" && ../../visualise.sh && cd .. && cd ..
echo Visualisation in: $base/maps/map.gif
cd "$base/paths" && ../../log-paths.sh
echo Commands and logs in: $base/paths/

