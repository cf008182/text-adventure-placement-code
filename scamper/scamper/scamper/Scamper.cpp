#include <stdlib.h>
#include <iostream>
#include <bitset>
#include <vector>
#include <deque>
#include <queue>
#include <stack>
#include <algorithm>
#include <unordered_set>
#include <assert.h>
#include <string.h>
#include <ctime>

#include <fstream>
#include <unistd.h>

#include "Scott.h"
#include "db.h"


// Hashing based on Boost, from: https://stackoverflow.com/questions/2590677/how-do-i-combine-hash-values-in-c0x
inline void hash_combine(std::size_t& seed) { }

template <typename T, typename... Rest>
inline void hash_combine(std::size_t& seed, const T& v, Rest... rest) {
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
    hash_combine(seed, rest...);
}

// Hash range from Boost:
template <class It>
inline std::size_t hash_range(It first, It last)
{
    std::size_t seed = 0;

    for(; first != last; ++first)
    {
        hash_combine(seed, *first);
    }

    return seed;
}


#define MAKE_HASHABLE(type, ...) \
    namespace std {\
        template<> struct hash<type> {\
            std::size_t operator()(const type &t) const {\
                std::size_t ret = 0;\
                hash_combine(ret, __VA_ARGS__);\
                return ret;\
            }\
        };\
    }

// End of custom hashing.

// Memory usage, from https://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-runtime-using-c
long process_mem_usage()
{
    int tSize = 0, resident = 0, share = 0;
    std::ifstream buffer("/proc/self/statm");
    buffer >> tSize >> resident >> share;
    buffer.close();

    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
    long rss = resident * page_size_kb;

    //long shared_mem = share * page_size_kb;
    
    return rss;
}

int process_mem_usage_mb()
{
    return process_mem_usage() / 1024;
}

using namespace std;

// A verb/noun pair, which forms a command in the game.
class command {
    public:
        uint8_t vb;
        uint8_t no;

    inline bool operator==(const command& rhs){
        return (vb == rhs.vb) && (no == rhs.no);
    }
};

// Null command: verb 0; noun 0.
command null_cmd = {0, 0};

// Is the command null?
bool is_null_cmd(command c) {
    return c.vb==0 && c.no==0;
}

// A GameState in the game tree.
typedef struct {
    GameState q;
    size_t parent; // ID of parent node.
    command cmd; // Command to get from parent node to here.
} TreeState;



// Various globals from ScottCurses.cpp that we can access:

// Did an error occur during the most recent command?
extern int err;
// Which messages were output during the most recent command?
extern vector<uint8_t> messages_seen;
// Which actions were executed during the most recent command?
extern vector<uint8_t> actions_seen;
// Which conditions were true/false after executing the most recent command?
extern vector<uint8_t> conditions_seen;

// Initialise the game to its initial state and look.
// Usually prints a welcome message in the game.
extern GameState InitGame();
// Execute a single command from a given GameState.
extern GameState NextState(GameState q0, int vb, int no);

// Count how many distinct condition combinations there are in the game's actions.
int conditions = 0;
extern int CountConditions();

// Which messages have been seen at any point in exploration?
bitset<MAX_MESSAGE+1> messages_reached;
// Which actions have been executed at any point in exploration?
bitset<MAX_ACTION+1> actions_reached;
// Which rooms have been entered at any point in exploration?
bitset<MAX_ROOM+1> rooms_reached;
// Which condition value combinations have been reached at any point in exploration?
vector<bitset<32>> conditions_reached(MAX_ACTION+1);
int conditions_reached_count = 0;
// Storage for tree of all states explored, indexable by ID.
vector<TreeState> states;

// Track time elapsed for stats.
std::clock_t c_start = std::clock();

// Have we found the next waypoint?
deque<int> waypoints;
bool found_waypoint = false;
size_t qway_id;

// A "reference" to a TreeState.
// Comparison ignores state of random number generator.
// The reference is just an integer, indexing into states.
class TreeStateRef {
    public:
        size_t n;

    inline bool operator==(const TreeStateRef& rhs) const{
        // Using the index, get the GameState corresponding to the TreeStateRef.
        GameState q1 = states[n].q;
        GameState q2 = states[rhs.n].q;
        // Check equality on all fields of the GameState, except random number generation.
        return
            (q1.BitFlags == q2.BitFlags) &&
            (q1.GameOver == q2.GameOver) &&
            (q1.PlayerRoom == q2.PlayerRoom) &&
            (q1.LightTime == q2.LightTime) &&
            (q1.CurrentCounter == q2.CurrentCounter) &&
            (q1.SavedRoom == q2.SavedRoom) &&
            std::equal(q1.Counters, q1.Counters+16, q2.Counters) &&
            std::equal(q1.RoomSaved, q1.RoomSaved+16, q2.RoomSaved) &&
            std::equal(q1.ItemLocs, q1.ItemLocs+MAX_ITEM+1, q2.ItemLocs);
    }

};

// Hash function for TreeStateRef.
// Respects equality on GameState as defined above, ignoring random number generation.
namespace std {
    template<> struct hash<TreeStateRef> {
        std::size_t operator()(const TreeStateRef &t) const {
            std::size_t ret = 0;
            GameState q = states[t.n].q;
            hash_combine(ret, q.BitFlags, q.GameOver, q.PlayerRoom, q.LightTime,
                //q.RandState,
                q.CurrentCounter, q.SavedRoom,
                hash_range(q.Counters, q.Counters+16),
                hash_range(q.RoomSaved, q.RoomSaved+16),
                hash_range(q.ItemLocs, q.ItemLocs+MAX_ITEM+1)
            );
            return ret;
        }
    };
}

// Maintain a hashtable of GameStates, so we can discard repetitions.
std::unordered_set<TreeStateRef> state_hash;

// Hard-coded commands for navigation.
vector<command> move_commands{
    {VERB_GO, NOUN_NORTH}, {VERB_GO, NOUN_SOUTH},
    {VERB_GO, NOUN_EAST}, {VERB_GO, NOUN_WEST},
    {VERB_GO, NOUN_UP}, {VERB_GO, NOUN_DOWN},
    };

// Commands that activate actions in the game.
vector<command> action_commands;

// Prototypes for functions in this file.
GameState NextState(GameState q0, command c);
bool record_new();
void log_map();
void dump_map(FILE* out);
void dump_state(TreeState t);
void explore_pure_random(size_t n1);

// Call NextState with a command object, rather than a verb/noun pair.
GameState NextState(GameState q0, command c) {
    // Just extract the verb/noun from the command and pass on.
    return NextState(q0, c.vb, c.no);
}

// Look at which messages/actions were recorded during the next state function.
// Flag them as having been seen.
// Return whether anything new was seen, except rooms.
bool record_new(GameState q) {
    // New and significant.
    bool novel = false;
    // Worth logging, but not using to guide search.
    bool seminovel = false;

    // If a new message was output, this state is novel.
    for (auto m : messages_seen) {
        if (!messages_reached[m]) {
            //cout << ((int) *it) << " : " << Messages[*it] << endl;
            messages_reached[m] = true;
            novel = true;
        }
    }

    // If a new action was activated, this state is novel.
    for (auto a : actions_seen) {
        if (!actions_reached[a]) {
            //cout << ((int) *it) << endl;
            actions_reached[a] = true;
            novel = true;
        }
        
        if (!waypoints.empty() && a == waypoints.front() && !found_waypoint) {
            cout << "Found waypoint " << a << endl;
            qway_id = states.size()-1;
            found_waypoint = true;
        }

    }
    
    // If a new combination of action conditions was found, this state is novel.
    for (int n = 0; n <= MAX_ACTION; n++) {
        if (!conditions_reached[n][conditions_seen[n]]) {
            conditions_reached[n][conditions_seen[n]] = true;
            conditions_reached_count++;
            novel = true;
        }
    }

    // If a new room was reached, this state is semi-novel.
    if (!rooms_reached[q.PlayerRoom]) {
        rooms_reached[q.PlayerRoom] = true;
        seminovel = true;
    }

    // Novel and semi-novel states get logged.
    if (novel || seminovel) {
        log_map();    
    }

    return novel;    
}

// Numerical ID to use in filename of next output play.
int pathnum = 0;

// Save a text file listing the commands to reach a game state of interest.
void dump_state(TreeState t) {
    char filename[32];
    snprintf(filename, 32, "paths/%08d.txt", pathnum);
    FILE* p = fopen(filename, "w");
    if (p == NULL) {
        fprintf(stderr, "Can't open: %s\n", filename);
        exit(1);
    }
    // Trace from the state to the root of the tree, recording commands.
    stack<TreeState> steps;
    while (!is_null_cmd(t.cmd)) {
        steps.push(t);
        t = states[t.parent];
    }
    // Now print the commands in the file.
    while (!steps.empty()) {
        t = steps.top();
        fprintf(p, "%s %s\n", Verbs[t.cmd.vb], t.cmd.no == 0 ? "" : Nouns[t.cmd.no]);
        steps.pop();
    }
    // Add "quit" so that, if this is passed back to an interpreter, it terminates gracefully.
    // Caution: some games might not recognise "quit".
    fprintf(p, "quit\n");
    fclose(p);
    // Increment the ID for the next filename.
    pathnum++;
}

// Look at the actions in the game database.
// Record which commands activate them.
void populate_action_commands() {
    for (int n = 0; n <= MAX_ACTION; n++) {
        int v = Actions[n].Vocab;
        uint8_t vb = v / 150;
        uint8_t no = v % 150;
        if (vb == 0) {
            continue;
        }
        command c = {vb, no};
        // This was so rare as to be of almost no value.
        // Almost all scripted actions involve some other outcome, such as looking.
        //if (Actions[n].Action[0] == (150 * 54) && (Actions[n].Action[1] == 0)) {
        //    fprintf(stdout, "Pure movement action: %s %s\n", Verbs[vb], no == 0 ? "" : Nouns[no]);
        //}
        if (std::find(action_commands.begin(), action_commands.end(), c) == action_commands.end()) {
            action_commands.push_back(c);
        }
    }
}

// Map from item ID to "autoget" noun for get/drop.
uint8_t item2noun[MAX_ITEM + 1];
// List of items with "autoget" text.
vector<uint8_t> gettables;
// Record gettable items in the game database.
void populate_item_nouns() {
    for (int n = 0; n <= MAX_ITEM; n++) {
        // Default: Item has no "autoget" associated noun.
        item2noun[n] = 255;
        if (Items[n].AutoGet == NULL) {
            continue;
        }
        
        // If an item has "autoget" text, find the corresponding noun, if any.
        for (int m = 0; m <= MAX_WORD; m++) {
            if (Nouns[m] == NULL) {
                continue;
            }
            if (strncmp(Items[n].AutoGet, Nouns[m], WORD_LENGTH)==0) {
                // Record it as a "gettable" item.
                gettables.push_back(n);
                item2noun[n] = m;
                break;
            }
        }
    }
}

// Most stupid possible random exploration strategy.
void explore_pure_random(size_t n1) {
    uint8_t vb = rand() % (MAX_WORD + 1);
    uint8_t no = rand() % (MAX_WORD + 1);
    GameState q2 = NextState(states[n1].q, vb, no);
    TreeState t2 = {q2, n1, {vb, no}};
    states.push_back(t2);
    if (record_new(q2)) {
        dump_state(t2);
    }
}

// Better, but still stupid, random exploration strategy.
bool explore_random(size_t n1) {
    GameState q1 = states[n1].q;
    GameState q2;
    command c;
    if (q1.GameOver) {
        return false;
    }
    switch (rand() % 4) {
        case 0:
            // Move.
            c = move_commands[rand() % 6];
            q2 = NextState(q1, c);
            break;
        case 1:
            // Activate action.
            c = action_commands[rand() % action_commands.size()];
            q2 = NextState(q1, c);
            break;
        case 2:
            // Take object.
            c = {VERB_GET, item2noun[gettables[rand() % gettables.size()]]};
            q2 = NextState(q1, c);
            break;
        case 3:
            // Drop object.
            c = {VERB_DROP, item2noun[gettables[rand() % gettables.size()]]};
            q2 = NextState(q1, c);
            break;
        default:
            assert(false);
            return false;
    }

    if (err) {
        return false;
    }

    TreeState t2 = {q2, n1, c};
    states.push_back(t2);
    if (record_new(q2)) {
        dump_state(t2);
    }
    return true;
}

queue<size_t> novel_states;
bool expand_one(const size_t& n1, const command& c) {
    // Evaluate the command.
    GameState q2 = NextState(states[n1].q, c);
    // Immediately discard the result if an error occurred.
    if (err) {
        return false;
    }
    // Now add the new state to the game tree.
    TreeState t2 = {q2, n1, c};
    states.push_back(t2);
    TreeStateRef tr2 = {states.size()-1};

    // If it was novel, dump it and add it to the queue of novel states.
    if (record_new(q2)) {
        dump_state(t2);
        novel_states.push(states.size()-1);
        // Don't care whether it was already in the tree if it's novel.
        state_hash.insert(tr2);
        return true;
    }

    // Check whether the state was already in the game tree.
    if (state_hash.find(tr2) != state_hash.end()) {
        states.pop_back();
        return false;
    }
    state_hash.insert(tr2); // Should optimise this to remove the check above.
    return true;
}

vector<size_t> unexpanded;

void expand(size_t n1) {
    command c; // Temporary for command to try.
    vector<int> room_states; // States in different rooms.
    vector<int> new_states; // Newly created states from this expansion.
    queue<int> location_states; // States reached by motion from expanded location.
    bitset<MAX_ROOM+1> locations; // Locations reached by motion.
    
    if (states[n1].q.GameOver) {
        return;
    }

    // Initialise queue with current state and record its location.
    location_states.push(n1);
    room_states.push_back(n1);
    locations[states[n1].q.PlayerRoom] = true;

    // Move.
    while (!location_states.empty()) {
        size_t n = location_states.front();
        location_states.pop();

        for (int m = 0; m < 6; m++) {
            c = move_commands[m];
            if (!expand_one(n, c)) {
                continue;
            }

            size_t new_state = states.size()-1;
            new_states.push_back(new_state);

            // Only queue up the new state for actions if it reached a new room.
            uint8_t r = states[new_state].q.PlayerRoom;
            if (!locations[r] && !states[new_state].q.GameOver) {
                locations[r] = true;
                location_states.push(new_state);
                room_states.push_back(new_state);
            }
            
        }
    }

    // Activate action.    
    for (auto n : room_states) {
        for (size_t m = 0; m < action_commands.size(); m++) {
            c = action_commands[m];
            if (!expand_one(n, c)) {
                continue;
            }
            new_states.push_back(states.size()-1);
        }
    }

    // Take object.
    for (auto n : room_states) {
        for (size_t m = 0; m < gettables.size(); m++) {
            c = {VERB_GET, item2noun[gettables[m]]};
            if (!expand_one(n, c)) {
                continue;
            }
            new_states.push_back(states.size()-1);
        }
    }
    
    // Drop object.
    for (auto n : room_states) {
        for (size_t m = 0; m < gettables.size(); m++) {
            c = {VERB_DROP, item2noun[gettables[m]]};
            if (!expand_one(n, c)) {
                continue;
            }
            new_states.push_back(states.size()-1);
        }
    }

    // Record all states as potentially expandable.
    for (auto n : new_states) {
        unexpanded.push_back(n);
    }

    return;

}

// How many, and what percentage, of rooms/messages/actions have been found?
// Log this on standard output.
int percent(int x, int y) {
    if (y == 0)
        return 0;
    
    return (100 * x) / y;
}

void log_progress() {
    printf("Rooms reached:       %3zu/%3d (%3d%%)\n", rooms_reached.count(), MAX_ROOM + 1, percent(rooms_reached.count(),MAX_ROOM+1));
    printf("Messages reached:    %3zu/%3d (%3d%%)\n", messages_reached.count(), MAX_MESSAGE + 1, percent(messages_reached.count(),MAX_MESSAGE+1));
    printf("Actions reached:     %3zu/%3d (%3d%%)\n", actions_reached.count(), MAX_ACTION + 1, percent(actions_reached.count(),MAX_ACTION+1));
    printf("Conditions reached:  %3d/%3d (%3d%%)\n", conditions_reached_count, conditions, percent(conditions_reached_count,conditions));
    printf("States explored:     %zu\n", states.size());
    printf("Seconds elapsed:     %d\n", (int) ((std::clock()-c_start) / CLOCKS_PER_SEC));
    printf("Memory used (MB):    %d\n", process_mem_usage_mb());

    printf("\n");
}

void log_csv() {
    printf("%3zu, %3d, %3d, ", rooms_reached.count(), MAX_ROOM + 1, percent(rooms_reached.count(),MAX_ROOM+1));
    printf("%3zu, %3d, %3d, ", messages_reached.count(), MAX_MESSAGE + 1, percent(messages_reached.count(),MAX_MESSAGE+1));
    printf("%3zu, %3d, %3d, ", actions_reached.count(), MAX_ACTION + 1, percent(actions_reached.count(),MAX_ACTION+1));
    printf("%3d, %3d, %3d, ", conditions_reached_count, conditions, percent(conditions_reached_count,conditions));
    printf("%zu, ", states.size());
    printf("%d, ", (int) ((std::clock()-c_start) / CLOCKS_PER_SEC));
    printf("%d", process_mem_usage_mb());

    printf("\n");
}


// ID to use in next dumped map.
int mapnum = 0;
// Save a "map", recording everything that has been reached so far visually.
// These maps can be combined to produce an animated visualisation of the search.
void log_map() {
    // Create the file.
    char filename[32];
    snprintf(filename, 32, "maps/%08d.dot", mapnum);
    FILE* out = fopen(filename, "w");
    if (out == NULL) {
        fprintf(stderr, "Can't open: %s\n", filename);
        exit(1);
    }

    // Dump the map.
    dump_map(out);

    // Increment the filename counter for the next map.
    mapnum++;
    fclose(out);
}

#define STRATEGY 3

int main() {
    cout << "(ScAmPER) Scott Adams exPlicitly Evaluating Reachability" << endl << endl;

    cout << "Collecting action commands." << endl;
    populate_action_commands();
    cout << "Found " << action_commands.size() << " distinct action triggers." << endl;
    cout << "Counting action conditions." << endl;
    conditions = CountConditions();
    cout << "Found " << conditions << " conditions in action commands." << endl;
    cout << "Collecting gettable items." << endl;
    populate_item_nouns();
    cout << "Found " << gettables.size() << " gettable items." << endl;

    cout << "GAMESIZE:, ";
    cout << MAX_ITEM << ", " << MAX_ACTION << ", " << MAX_WORD << ", " << MAX_ROOM << ", ";
    cout << action_commands.size() << ", " << conditions << ", " << gettables.size() << endl;

    cout << "Dumping map." << endl;
    {
        FILE* out = fopen("map.dot", "w");
        dump_map(out);
        fclose(out);
    }
    cout << "Done dumping map." << endl;

    cout << "Reading waypoints from stdin." << endl;
    {
        int n;
        while (cin >> n) {
            waypoints.push_back(n);
        }
    }

    cout << "Target waypoints: ";
    for (auto it = waypoints.begin(); it != waypoints.end(); it++) {
        cout << (*it) << " ";
    }
    cout << endl;

    // Initialise game state.
    GameState q0 = InitGame();
    TreeState t0 = {q0, 0, null_cmd};
    states.push_back(t0);
    TreeStateRef tr0 = {0};
    state_hash.insert(tr0);
    // Record seen messages.
    if(record_new(q0)) {
        dump_state(t0);
    }

    if (STRATEGY == 1) {
        // If anything new, dump the path.
        // Try a new action. Repeat.
        for (int n = 0; n < 1000000; n++) {
            if(states.back().q.GameOver) {
                break;
            }
            explore_pure_random(states.size() - 1);
        }
        
    }

    if (STRATEGY == 2) {
        // 40000000 - approx 8 GB, 10 min on Pirate Adventrue
        // 1000000 - enough for tutorial 4, but not 5
        for (int n = 0; n < 10000000;) {
            int m = rand() % states.size();
            if (explore_random(m)) {
                n++;
            }
        }
        
    }

    if (STRATEGY == 3) {
        unexpanded.push_back(0);
        size_t last_expanded_novel = 1;
        
        while(true) {
            for (int n = 0; ; n++) {
                if (n % 10 == 0) {
                    cout << "Cycle " << n << endl;
                    log_progress();
                }
                size_t n1;
                if (found_waypoint) {
                    cout << "Breaking cycle as waypoint found." << endl;
                    break;
                }
                if (novel_states.empty() && unexpanded.empty()) {
                    cout << "Finished!" << endl;
                    break;
                }
                else if (novel_states.empty()) {
                    int mb = process_mem_usage_mb();
                    if (mb >= 8192 || (states.size() > 100000 && (last_expanded_novel * 2) < states.size())) {
                        cout << "Stagnated." << endl;
                        break;
                    }
                
                    int m = rand() % unexpanded.size();
                    n1 = unexpanded[m];
                    unexpanded[m] = unexpanded[unexpanded.size()-1]; // Faster than erasing and maintaining order.
                    unexpanded.pop_back();
                }
                else {
                    last_expanded_novel = states.size();
                    n1 = novel_states.front();
                    novel_states.pop();
                }

                {
                    // What is a sensible value for depth?
                    // 0 is fast.
                    // 1 and 2 are slower, but slightly better.
                    // 3 seems very slow with little or no further benefit.
                    int depth = 2;
                    size_t x = unexpanded.size();
                    // Expand the chosen state.
                    expand(n1);
                    // Retrieve all the new, unexpanded states and expand them too.

                    while (depth > 0) {
                        std::vector<size_t> explore;
                        while (unexpanded.size() != x) {
                            explore.push_back(unexpanded.back());
                            unexpanded.pop_back();
                        }
                        x = unexpanded.size();
                        for (auto n2 : explore) {
                            expand(n2);
                        }
                        depth--;
                    }

                }
            }
            
            if (found_waypoint) {
                cout << "Removing waypoint " << waypoints.front() << endl;
                found_waypoint = false;
                waypoints.pop_front();
                if (waypoints.empty()) {
                    cout << "All waypoints found." << endl;
                    break;
                }
                cout << "Restarting with waypoint " << waypoints.front() << endl;
                novel_states = {};
                novel_states.push(qway_id);
                //unexpanded.clear();
                //unexpanded.push_back(qway_id);

                messages_reached.reset();
                actions_reached.reset();
                rooms_reached.reset();
                for (auto c : conditions_reached) {
                    c.reset();
                }
                conditions_reached_count = 0;
                last_expanded_novel = states.size();

            }
            else {
                break;
            }
        }
    }

    log_progress();
    cout << "Terminating normally." << endl;
    log_csv();
}

// Dump up to n characters of string s to stream out.
// Only dump alphanumerics and spaces.
// Tabs and so on are transliterated to space.
void dump_string_prefix(FILE* out, char* s, int n) {
    if (s == NULL) {
        return;
    }
    while (*s != 0 && n > 0) {
        if (isalnum(*s)) {
            fputc(*s, out);
            n--;
        }
        if (isspace(*s)) {
            fputc(' ', out);
            n--;
        }
        s++;
    }
}

// Names of exits, in order.
char const* exit_names[] = { "n", "s", "e", "w", "u", "d" };
// Corresponding directions in the map, indicating where edges should attach to nodes.
// Note up/down becomes northeast/southwest.
char const* src_ports[]  = { "n", "s", "e", "w", "ne", "sw" };
// Reverse directions, in case we want to flip the direction of the edge for ranking constraints.
char const* targ_ports[] = { "s", "n", "w", "e", "sw", "ne" };
// Dump a map of what has been reached in Graphviz dot format to the stream out.
void dump_map(FILE* out) {
    fprintf(out, "digraph map {\n");
    fprintf(out, "node [shape = box, fillcolor = gray];\n");
    // Dump the map of rooms.
    for (int r = 0; r <= MAX_ROOM; r++) {
        fprintf(out, "room%d [label=\"%d: ", r, r);
        dump_string_prefix(out, Rooms[r].Text, 20);
        fprintf(out, "\"");
        // Reached rooms are displayed as filled.
        if (rooms_reached[r]) {
            fprintf(out, " style=filled");        
        }
        fprintf(out, "];\n");
        // Generate edges to connect rooms.
        for (int x = 0; x < 6; x++) {
            int dest = Rooms[r].Exits[x];
            if (dest == 0) {
                continue;
            }
//            if ((x==2) || (x==3)) {
//                fprintf(out, "room%d -> room%d [label=\"%s\" tailport=\"%s\" headport=\"%s\" constraint=false];\n", r, dest, exit_names[x], src_ports[x], targ_ports[x]);
//                continue;
//            }
            // Exits north/east/up generate rank constraints.
            if (x % 2 == 1) {
                fprintf(out, "room%d -> room%d [label=\"%s\" tailport=\"%s\" headport=\"%s\"];\n", r, dest, exit_names[x], src_ports[x], targ_ports[x]);
            }
            // Exits going south/west/down generate rank constraints in the reverse direction.
            else {
//                fprintf(out, "room%d -> room%d [label=\"%s\" tailport=\"%s\" headport=\"%s\" constraint=false];\n", r, dest, exit_names[x], src_ports[x], targ_ports[x]);
                fprintf(out, "room%d -> room%d [label=\"%s\" tailport=\"%s\" headport=\"%s\" dir=back];\n", dest, r, exit_names[x], targ_ports[x], src_ports[x]);
            }
        }
        
    }

    // Output a grid showing which messages have been reached.
    {
        fprintf(out, "messages [shape=none label =<<table border=\"0\" cellspacing=\"0\">\n");
        fprintf(out, "<tr><td border=\"1\" colspan=\"10\">Messages</td></tr>\n");
        int m = 0;
        while ((m <= MAX_MESSAGE) || (m % 10 !=0)) {
            if (m % 10 == 0) {
                fprintf(out, "<tr>");
            }
            if (m > MAX_MESSAGE) {
                fprintf(out, "<td border=\"1\"> </td>");
            }
            else if (messages_reached[m]) {
                fprintf(out, "<td border=\"1\" bgcolor=\"gray\">%d</td>", m);
            }
            else {
                fprintf(out, "<td border=\"1\">%d</td>", m);
            }
            m++;
            if (m % 10 == 0) {
                fprintf(out, "</tr>\n");
            }
        }
        fprintf(out, "</table>> ];\n");
    }

    // Output a grid showing which actions have been reached.
    {
        fprintf(out, "actions [shape=none label =<<table border=\"0\" cellspacing=\"0\">\n");
        fprintf(out, "<tr><td border=\"1\" colspan=\"10\">Actions</td></tr>\n");
        int a = 0;
        while ((a <= MAX_ACTION) || (a % 10 !=0)) {
            if (a % 10 == 0) {
                fprintf(out, "<tr>");
            }
            if (a > MAX_ACTION) {
                fprintf(out, "<td border=\"1\"> </td>");
            }
            else if (actions_reached[a]) {
                fprintf(out, "<td border=\"1\" bgcolor=\"gray\">%d</td>", a);
            }
            else {
                fprintf(out, "<td border=\"1\">%d</td>", a);
            }
            a++;
            if (a % 10 == 0) {
                fprintf(out, "</tr>\n");
            }
        }
        fprintf(out, "</table>> ];\n");
    }


/*    
    // Output a table of messages, including the prefix of each message.
    fprintf(out, "messages [shape=none label =<<table border=\"0\" cellspacing=\"0\">\n");
    for (int m = 0; m <= MAX_MESSAGE; m++) {
        if (messages_reached[m]) {
            fprintf(out, "<tr><td border=\"1\" bgcolor=\"gray\">%d</td><td border=\"1\">", m);
            dump_string_prefix(out, Messages[m], 20);
            fprintf(out, "</td></tr>\n");
        }
        else {
            fprintf(out, "<tr><td border=\"1\">%d</td><td border=\"1\">", m);
            dump_string_prefix(out, Messages[m], 20);
            fprintf(out, "</td></tr>\n");
        }
    }
    fprintf(out, "</table>> ];\n");
*/    
    fprintf(out, "}\n");
}

