#define MAX_ITEM 65
#define MAX_ACTION 169
#define MAX_WORD 69
#define MAX_ROOM 33
#define MAX_CARRY 6
#define PLAYER_START 11
#define TREASURES 13
#define WORD_LENGTH 3
#define MAX_MESSAGE 76
#define TREASURE_ROOM 3
#define LIGHT_REFILL 125

typedef struct {
	uint32_t BitFlags;
	uint8_t GameOver;
	uint8_t PlayerRoom;
	uint8_t LightTime;
	uint8_t RandState;
	int8_t CurrentCounter;
	uint8_t SavedRoom;
	int8_t Counters[16];
	uint8_t RoomSaved[16];
	uint8_t ItemLocs[MAX_ITEM + 1];
} GameState;
#ifdef INTERPRETER
Item Items[] = {
    {
        "Glowing *FIRESTONE*", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Dark hole", // Text
        4, // InitialLoc
        NULL, // AutoGet
    },
    {
        "*Pot of RUBIES*", // Text
        4, // InitialLoc
        "RUB", // AutoGet
    },
    {
        "Spider web with writing on it", // Text
        2, // InitialLoc
        NULL, // AutoGet
    },
    {
        "-HOLLOW- stump and remains of a felled tree", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Cypress tree", // Text
        1, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Water", // Text
        10, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Evil smelling mud", // Text
        1, // InitialLoc
        "MUD", // AutoGet
    },
    {
        "*GOLDEN FISH*", // Text
        10, // InitialLoc
        "FIS", // AutoGet
    },
    {
        "Lit brass lamp", // Text
        0, // InitialLoc
        "LAM", // AutoGet
    },
    {
        "Old fashioned brass lamp", // Text
        3, // InitialLoc
        "LAM", // AutoGet
    },
    {
        "Rusty axe (Magic word \"BUNYON\" on it)", // Text
        10, // InitialLoc
        "AXE", // AutoGet
    },
    {
        "Water in bottle", // Text
        3, // InitialLoc
        "BOT", // AutoGet
    },
    {
        "Empty bottle", // Text
        0, // InitialLoc
        "BOT", // AutoGet
    },
    {
        "Ring of skeleton keys", // Text
        2, // InitialLoc
        "KEY", // AutoGet
    },
    {
        "Sign \"Leave *TREASURES* here, then say: SCORE\"", // Text
        3, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Locked door", // Text
        5, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Open door with a hallway beyond", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Swamp gas", // Text
        1, // InitialLoc
        NULL, // AutoGet
    },
    {
        "*GOLDEN NET*", // Text
        18, // InitialLoc
        "NET", // AutoGet
    },
    {
        "Chigger bites", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Infected chigger bites", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Patches of \"OILY\" slime", // Text
        1, // InitialLoc
        "OIL", // AutoGet
    },
    {
        "*ROYAL HONEY*", // Text
        8, // InitialLoc
        "HON", // AutoGet
    },
    {
        "Large african bees", // Text
        8, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Very thin black bear", // Text
        21, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Bees in a bottle", // Text
        0, // InitialLoc
        "BOT", // AutoGet
    },
    {
        "Large sleeping dragon", // Text
        23, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Flint & steel", // Text
        30, // InitialLoc
        "FLI", // AutoGet
    },
    {
        "*Thick PERSIAN RUG*", // Text
        17, // InitialLoc
        "RUG", // AutoGet
    },
    {
        "Sign: \"magic word's AWAY! Look la...\"\n(Rest of sign is missing!)", // Text
        18, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Distended gas bladder", // Text
        0, // InitialLoc
        "BLA", // AutoGet
    },
    {
        "Bricked up window", // Text
        20, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Sign here says \"In many cases mud is good. In others...\"", // Text
        23, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Stream of lava", // Text
        18, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Bricked up window with a hole in it", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Loose fire bricks", // Text
        0, // InitialLoc
        "BRI", // AutoGet
    },
    {
        "*GOLD CROWN*", // Text
        22, // InitialLoc
        "CRO", // AutoGet
    },
    {
        "*MAGIC MIRROR*", // Text
        21, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Sleeping bear", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Empty wine bladder", // Text
        9, // InitialLoc
        "BLA", // AutoGet
    },
    {
        "Broken glass", // Text
        0, // InitialLoc
        "GLA", // AutoGet
    },
    {
        "Chiggers", // Text
        1, // InitialLoc
        "CHI", // AutoGet
    },
    {
        "Slightly woozy bear", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "*DRAGON EGGS* (very rare)", // Text
        0, // InitialLoc
        "EGG", // AutoGet
    },
    {
        "Lava stream with brick dam", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "*JEWELED FRUIT*", // Text
        25, // InitialLoc
        "FRU", // AutoGet
    },
    {
        "*Small statue of a BLUE OX*", // Text
        26, // InitialLoc
        "OX", // AutoGet
    },
    {
        "*DIAMOND RING*", // Text
        0, // InitialLoc
        "RIN", // AutoGet
    },
    {
        "*DIAMOND BRACELET*", // Text
        0, // InitialLoc
        "BRA", // AutoGet
    },
    {
        "Strange scratchings on rock says: \"ALADIN was here\"", // Text
        14, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Sign says \"LIMBO. Find right exit and live again!\"", // Text
        33, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Smoking hole. pieces of dragon and gore.", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Sign says \"No swimming allowed here\"", // Text
        10, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Arrow pointing down", // Text
        17, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Dead fish", // Text
        0, // InitialLoc
        "FIS", // AutoGet
    },
    {
        "*FIRESTONE* (cold now)", // Text
        0, // InitialLoc
        "FIR", // AutoGet
    },
    {
        "Sign says \"Paul's place\"", // Text
        25, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Trees", // Text
        11, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Sign here says \"Opposite of LIGHT is UNLIGHT\"", // Text
        12, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Empty lamp", // Text
        0, // InitialLoc
        "LAM", // AutoGet
    },
    {
        "Muddy worthless old rug", // Text
        0, // InitialLoc
        "RUG", // AutoGet
    },
    {
        "Large outdoor Advertisement", // Text
        29, // InitialLoc
        "ADV", // AutoGet
    },
    {
        "Hole", // Text
        29, // InitialLoc
        NULL, // AutoGet
    },
    {
        "", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
};
char* Verbs[] = {
    "AUT",
    "GO",
    "*ENT",
    "*RUN",
    "*WAL",
    "*CLI",
    "JUM",
    "AT",
    "CHO",
    "*CUT",
    "GET",
    "*TAK",
    "*PIC",
    "*CAT",
    "LIG",
    "*.",
    "*IGN",
    "*BUR",
    "DRO",
    "*REL",
    "*SPI",
    "*LEA",
    "*GIV",
    "*POU",
    "THR",
    "*TOS",
    "QUI",
    "SWI",
    "RUB",
    "LOO",
    "*EXA",
    "*DES",
    "STO",
    "SCO",
    "INV",
    "SAV",
    "WAK",
    "UNL",
    "REA",
    "ATT",
    "*SLA",
    "*KIL",
    "DRI",
    "*EAT",
    ".",
    "FIN",
    "*LOC",
    "HEL",
    "SAY",
    "*SPE",
    "*CAL",
    "SCR",
    "*YEL",
    "*HOL",
    ".",
    "FIL",
    "CRO",
    "DAM",
    "MAK",
    "*BUI",
    "WAV",
    "*TIC",
    "*KIC",
    "*KIS",
    "*TOU",
    "*FEE",
    "*FUC",
    "*HIT",
    "*POK",
    "OPE",
};
char* Nouns[] = {
    "ANY",
    "NORTH",
    "SOUTH",
    "EAST",
    "WEST",
    "UP",
    "DOWN",
    "NET",
    "FIS",
    "AWA",
    "MIR",
    "AXE",
    "*AX",
    "WAT",
    "BOT",
    "*CON",
    "HOL",
    "LAM",
    "SPI",
    "WIN",
    "DOO",
    "MUD",
    "*MED",
    "BEE",
    "ROC",
    "GAS",
    "FLI",
    "EGG",
    "OIL",
    "*SLI",
    "KEY",
    "HEL",
    "BUN",
    "INV",
    "LED",
    "THR",
    "CRO",
    "BRI",
    "BEA",
    "DRA",
    "RUG",
    "RUB",
    "HON",
    "FRU",
    "OX",
    "RIN",
    "CHI",
    "*BIT",
    "BRA",
    "SIG",
    "BLA",
    "WEB",
    "*WRI",
    "SWA",
    "LAV",
    "*DAM",
    "HAL",
    "TRE",
    "*STU",
    "FIR",
    "SHO",
    "*BAN",
    "ADV",
    "GLA",
    "ARO",
    "GAM",
    "BOO",
    "CHA",
    "LAK",
    "YOH",
};
Room Rooms[] = {
    {
        "", // Text
        {0, 7, 10, 1, 0, 24, }
    },
    {
        "dismal swamp", // Text
        {23, 0, 29, 25, 0, 0, }
    },
    {
        "top of a tall cypress tree", // Text
        {0, 0, 0, 0, 0, 1, }
    },
    {
        "damp hollow stump in the swamp", // Text
        {0, 0, 0, 0, 1, 4, }
    },
    {
        "root chamber under the stump", // Text
        {0, 0, 0, 0, 3, 0, }
    },
    {
        "semi-dark hole by the root chamber", // Text
        {0, 0, 0, 0, 4, 0, }
    },
    {
        "long down sloping hall", // Text
        {0, 0, 0, 0, 5, 7, }
    },
    {
        "large cavern", // Text
        {31, 9, 0, 27, 6, 12, }
    },
    {
        "large 8 sided room", // Text
        {0, 31, 0, 0, 0, 0, }
    },
    {
        "royal anteroom", // Text
        {7, 0, 0, 0, 20, 0, }
    },
    {
        "*I'm on the shore of a lake", // Text
        {26, 29, 0, 23, 0, 0, }
    },
    {
        "forest", // Text
        {11, 11, 23, 11, 0, 0, }
    },
    {
        "maze of pits", // Text
        {13, 15, 15, 0, 0, 13, }
    },
    {
        "maze of pits", // Text
        {0, 0, 0, 14, 12, 0, }
    },
    {
        "maze of pits", // Text
        {17, 12, 13, 16, 16, 17, }
    },
    {
        "maze of pits", // Text
        {12, 0, 13, 12, 13, 0, }
    },
    {
        "maze of pits", // Text
        {0, 17, 0, 0, 14, 17, }
    },
    {
        "maze of pits", // Text
        {17, 12, 12, 15, 14, 18, }
    },
    {
        "*I'm at the bottom of a very deep chasm. High above me is\na pair of ledges. One has a bricked up window across its face\nthe other faces a Throne-room", // Text
        {0, 0, 0, 0, 17, 0, }
    },
    {
        "*I'm on a narrow ledge by a chasm. Across the chasm is\nthe Throne-room", // Text
        {0, 0, 0, 20, 0, 0, }
    },
    {
        "royal chamber", // Text
        {0, 0, 0, 0, 0, 9, }
    },
    {
        "*I'm on a narrow ledge by a Throne-room\nAcross the chasm is another ledge", // Text
        {0, 0, 0, 0, 0, 0, }
    },
    {
        "throne room", // Text
        {0, 0, 0, 21, 0, 0, }
    },
    {
        "sunny meadow", // Text
        {0, 1, 10, 11, 0, 0, }
    },
    {
        "*I think I'm in real trouble now. There's a fellow here with\na pitchfork and pointed tail. ...Oh Hell!", // Text
        {0, 0, 0, 0, 0, 0, }
    },
    {
        "hidden grove", // Text
        {11, 0, 1, 0, 0, 0, }
    },
    {
        "quick-sand bog", // Text
        {0, 0, 0, 0, 0, 0, }
    },
    {
        "Memory chip of a COMPUTER!\nI took a wrong turn!", // Text
        {0, 0, 7, 0, 0, 0, }
    },
    {
        "top of an oak.\nTo the East I see a meadow, beyond that a lake.", // Text
        {0, 0, 0, 0, 0, 11, }
    },
    {
        "*I'm at the edge of a BOTTOMLESS hole", // Text
        {10, 0, 0, 1, 0, 0, }
    },
    {
        "*I'm on a ledge just below the rim of the BOTTOMLESS hole. I\ndon't think I want to go down", // Text
        {0, 0, 0, 0, 29, 24, }
    },
    {
        "long tunnel. I hear buzzing ahead", // Text
        {8, 7, 0, 0, 0, 0, }
    },
    {
        "*I'm in an endless corridor", // Text
        {32, 33, 32, 32, 32, 32, }
    },
    {
        "large misty room with strange\nunreadable letters over all the exits.", // Text
        {32, 24, 11, 24, 28, 24, }
    },
};
Action Actions[] = {
    {75, {161, 386, 160, 200, 0}, {17612, 0}},
    {10, {421, 667, 0, 0, 0}, {2011, 0}},
    {10, {401, 420, 400, 146, 0}, {1874, 8850}},
    {8, {523, 520, 260, 349, 0}, {2622, 0}},
    {100, {108, 760, 820, 420, 100}, {8312, 9064}},
    {100, {484, 0, 0, 0, 0}, {5613, 0}},
    {5, {141, 140, 20, 246, 0}, {6062, 0}},
    {8, {406, 426, 400, 842, 146}, {11145, 0}},
    {8, {482, 152, 0, 0, 0}, {2311, 0}},
    {100, {104, 308, 0, 0, 0}, {8626, 0}},
    {50, {161, 246, 160, 1100, 0}, {7259, 7800}},
    {100, {148, 140, 940, 500, 0}, {9062, 9900}},
    {30, {841, 426, 406, 400, 146}, {11145, 0}},
    {50, {542, 143, 0, 0, 0}, {10504, 9150}},
    {100, {248, 642, 720, 640, 700}, {8005, 7950}},
    {100, {248, 542, 1040, 540, 0}, {8005, 0}},
    {100, {28, 49, 20, 40, 0}, {6360, 8700}},
    {100, {288, 260, 280, 0, 0}, {11160, 9150}},
    {100, {248, 240, 0, 0, 0}, {9660, 0}},
    {100, {269, 260, 0, 0, 0}, {16558, 17357}},
    {100, {28, 48, 20, 40, 0}, {4110, 9000}},
    {100, {320, 328, 1200, 180, 0}, {9072, 11400}},
    {100, {524, 583, 580, 1220, 0}, {10800, 0}},
    {4404, {682, 0, 0, 0, 0}, {6900, 0}},
    {4407, {82, 0, 0, 0, 0}, {6900, 0}},
    {1521, {142, 421, 420, 140, 0}, {8902, 17703}},
    {1542, {462, 146, 482, 0, 0}, {2311, 0}},
    {1521, {142, 401, 400, 140, 0}, {8902, 17703}},
    {2742, {461, 460, 502, 780, 500}, {8864, 8005}},
    {2742, {461, 460, 0, 0, 0}, {7950, 0}},
    {1523, {482, 146, 0, 0, 0}, {2311, 0}},
    {1523, {482, 141, 266, 0, 0}, {2400, 0}},
    {1523, {482, 141, 261, 260, 520}, {10918, 0}},
    {1533, {0, 0, 0, 0, 0}, {9900, 0}},
    {8454, {364, 0, 0, 0, 0}, {7650, 0}},
    {5100, {0, 0, 0, 0, 0}, {9900, 0}},
    {7209, {581, 344, 460, 0, 0}, {8118, 8614}},
    {2100, {566, 0, 0, 0, 0}, {2850, 0}},
    {2125, {621, 561, 620, 0, 0}, {3021, 9209}},
    {8716, {523, 340, 0, 0, 0}, {8818, 0}},
    {2125, {622, 561, 620, 240, 0}, {10555, 8720}},
    {184, {404, 702, 380, 0, 0}, {8170, 9600}},
    {1525, {24, 806, 0, 0, 0}, {2400, 0}},
    {1525, {24, 801, 800, 620, 0}, {10918, 0}},
    {2725, {621, 620, 800, 0, 0}, {10918, 3450}},
    {2125, {362, 561, 0, 0, 0}, {3300, 0}},
    {6803, {0, 0, 0, 0, 0}, {17100, 0}},
    {185, {384, 0, 0, 0, 0}, {3750, 0}},
    {1510, {762, 760, 505, 0, 0}, {7918, 0}},
    {2710, {761, 760, 582, 20, 0}, {7986, 8700}},
    {6343, {921, 920, 0, 0, 0}, {509, 0}},
    {1513, {122, 261, 260, 240, 0}, {8902, 17700}},
    {900, {384, 420, 726, 0, 0}, {8164, 0}},
    {900, {424, 380, 0, 0, 0}, {8164, 0}},
    {185, {424, 502, 0, 0, 0}, {3900, 0}},
    {185, {424, 505, 440, 0, 0}, {8170, 9600}},
    {8754, {723, 0, 680, 900, 682}, {10853, 11400}},
    {204, {364, 0, 0, 0, 0}, {7650, 0}},
    {2723, {521, 502, 520, 480, 280}, {4259, 8008}},
    {1513, {122, 266, 0, 0, 0}, {2400, 0}},
    {5751, {62, 0, 0, 0, 0}, {300, 0}},
    {207, {40, 102, 0, 0, 0}, {8170, 9600}},
    {2713, {241, 240, 260, 367, 0}, {10918, 4350}},
    {8267, {443, 1201, 440, 1200, 0}, {8909, 6669}},
    {1257, {100, 102, 292, 80, 221}, {8303, 1050}},
    {10370, {104, 322, 286, 0, 0}, {900, 0}},
    {5570, {104, 322, 286, 0, 0}, {900, 0}},
    {3611, {221, 60, 220, 0, 0}, {4558, 7950}},
    {10370, {322, 281, 320, 340, 0}, {8303, 11400}},
    {8400, {0, 0, 0, 0, 0}, {3750, 0}},
    {900, {384, 721, 0, 0, 0}, {5011, 0}},
    {8604, {723, 0, 680, 900, 682}, {10853, 11400}},
    {1537, {722, 720, 0, 0, 0}, {7918, 4800}},
    {4800, {0, 0, 0, 0, 0}, {5100, 0}},
    {3900, {0, 0, 0, 0, 0}, {9813, 0}},
    {1510, {762, 502, 0, 0, 0}, {3900, 0}},
    {2710, {761, 585, 820, 760, 0}, {5303, 8850}},
    {1088, {68, 765, 60, 0, 0}, {18410, 16710}},
    {1089, {68, 60, 542, 0, 0}, {18339, 9000}},
    {4950, {0, 0, 0, 0, 0}, {9750, 0}},
    {7050, {401, 0, 0, 0, 0}, {10610, 17055}},
    {7050, {421, 0, 0, 0, 0}, {10610, 17055}},
    {184, {364, 0, 0, 0, 0}, {15300, 0}},
    {1554, {682, 0, 0, 0, 0}, {7650, 0}},
    {7650, {502, 860, 360, 500, 0}, {6212, 8250}},
    {2723, {521, 542, 480, 880, 540}, {8003, 8293}},
    {1069, {68, 60, 0, 0, 0}, {9001, 16607}},
    {10370, {342, 0, 0, 0, 0}, {9600, 0}},
    {166, {702, 380, 0, 0, 0}, {10554, 9600}},
    {1088, {68, 760, 100, 80, 762}, {8308, 4710}},
    {6761, {0, 0, 0, 0, 0}, {16614, 0}},
    {5400, {0, 0, 0, 0, 0}, {197, 0}},
    {207, {82, 60, 0, 0, 0}, {8170, 9600}},
    {1257, {102, 221, 100, 80, 281}, {8303, 1200}},
    {5888, {502, 0, 0, 0, 0}, {3947, 0}},
    {5889, {542, 0, 0, 0, 0}, {5897, 0}},
    {6313, {241, 240, 260, 0, 0}, {509, 7800}},
    {6313, {122, 0, 0, 0, 0}, {450, 0}},
    {6342, {463, 460, 0, 0, 0}, {509, 0}},
    {1070, {322, 68, 320, 340, 60}, {8303, 810}},
    {4050, {524, 10, 0, 0, 0}, {4950, 0}},
    {4050, {524, 11, 200, 0, 0}, {8170, 9600}},
    {1200, {226, 0, 0, 0, 0}, {5700, 0}},
    {7232, {943, 221, 220, 500, 140}, {12768, 9358}},
    {7232, {221, 527, 220, 500, 0}, {12768, 9366}},
    {4217, {183, 0, 0, 0, 0}, {7650, 0}},
    {1521, {142, 140, 0, 0, 0}, {7918, 0}},
    {4217, {203, 169, 960, 160, 0}, {7403, 8700}},
    {4217, {203, 228, 0, 0, 0}, {150, 0}},
    {4217, {203, 208, 220, 960, 0}, {7558, 9209}},
    {4217, {203, 188, 200, 980, 0}, {7558, 9209}},
    {4217, {203, 168, 980, 180, 0}, {7403, 8700}},
    {7650, {401, 400, 420, 0, 0}, {462, 10800}},
    {7650, {421, 0, 0, 0, 0}, {463, 9150}},
    {4050, {527, 0, 0, 0, 0}, {15300, 0}},
    {9000, {0, 0, 0, 0, 0}, {150, 0}},
    {7232, {222, 0, 0, 0, 0}, {17785, 18600}},
    {2117, {183, 0, 0, 0, 0}, {1500, 0}},
    {6807, {0, 0, 0, 0, 0}, {15450, 0}},
    {2723, {521, 480, 520, 260, 0}, {8022, 17700}},
    {6780, {0, 0, 0, 0, 0}, {15450, 0}},
    {6771, {0, 0, 0, 0, 0}, {15450, 0}},
    {1110, {68, 60, 524, 220, 200}, {9062, 17700}},
    {207, {224, 560, 0, 0, 0}, {8170, 9600}},
    {7050, {524, 0, 0, 0, 0}, {16605, 16350}},
    {7050, {224, 0, 0, 0, 0}, {16605, 0}},
    {7050, {384, 0, 0, 0, 0}, {16605, 0}},
    {7050, {464, 0, 0, 0, 0}, {16606, 0}},
    {7050, {264, 0, 0, 0, 0}, {16609, 0}},
    {7050, {344, 0, 0, 0, 0}, {16609, 0}},
    {7050, {304, 0, 0, 0, 0}, {16609, 0}},
    {7050, {424, 0, 0, 0, 0}, {16605, 0}},
    {7050, {164, 0, 0, 0, 0}, {16608, 0}},
    {5570, {281, 322, 340, 320, 0}, {8005, 0}},
    {206, {342, 120, 0, 0, 0}, {8156, 10564}},
    {2117, {203, 200, 180, 0, 0}, {10810, 11400}},
    {5567, {183, 180, 200, 0, 0}, {10918, 1426}},
    {1551, {62, 0, 0, 0, 0}, {1711, 0}},
    {166, {1042, 480, 0, 0, 0}, {8170, 9600}},
    {1549, {0, 0, 0, 0, 0}, {16611, 0}},
    {2100, {561, 365, 0, 0, 0}, {3600, 0}},
    {7650, {0, 0, 0, 0, 0}, {150, 0}},
    {7209, {581, 347, 340, 667, 527}, {8118, 8464}},
    {7050, {24, 0, 0, 0, 0}, {16605, 0}},
    {3611, {226, 0, 0, 0, 0}, {5700, 0}},
    {7050, {404, 0, 0, 0, 0}, {16616, 15450}},
    {7232, {0, 0, 0, 0, 0}, {17785, 150}},
    {166, {84, 100, 0, 0, 0}, {8170, 9600}},
    {1542, {462, 460, 0, 0, 0}, {7918, 0}},
    {7050, {0, 0, 0, 0, 0}, {270, 0}},
    {1200, {0, 0, 0, 0, 0}, {197, 0}},
    {3600, {0, 0, 0, 0, 0}, {16800, 0}},
    {1050, {68, 60, 0, 0, 0}, {9122, 150}},
    {5315, {0, 0, 0, 0, 0}, {17771, 0}},
    {4200, {0, 0, 0, 0, 0}, {150, 0}},
    {7200, {0, 0, 0, 0, 0}, {17785, 150}},
    {6300, {0, 0, 0, 0, 0}, {17850, 0}},
    {2713, {241, 364, 240, 260, 0}, {15673, 10800}},
    {0, {2, 1120, 0, 0, 0}, {10800, 0}},
    {1559, {2, 0, 0, 0, 0}, {7650, 0}},
    {1559, {1122, 1120, 0, 0, 0}, {17752, 0}},
    {6750, {0, 0, 0, 0, 0}, {17100, 0}},
    {5762, {1243, 0, 0, 0, 0}, {10623, 0}},
    {4366, {0, 0, 0, 0, 0}, {6900, 0}},
    {900, {0, 0, 0, 0, 0}, {15300, 0}},
    {5868, {0, 0, 0, 0, 0}, {17100, 0}},
    {5850, {0, 0, 0, 0, 0}, {3750, 0}},
    {4350, {0, 0, 0, 0, 0}, {17825, 11400}},
    {1050, {0, 0, 0, 0, 0}, {18150, 0}},
    {166, {584, 600, 0, 0, 0}, {8176, 0}},
};
char* Messages[] = {
    "",
    "Nothing happens",
    "Chop 'er down!",
    "BOY that really hit the spot!",
    "Dragon smells something. Awakens & attacks me!",
    "Lock shatters",
    "I can't its locked",
    "TIMBER. Something fell from the tree top & vanished in the swamp",
    "TIMBER!",
    "Lamp is off",
    "Lamp burns with a cold flameless blue glow.",
    "I'm bit by a spider",
    "\nMy chigger bites are now INFECTED!\n",
    "My bites have rotted my whole body!",
    "Bear eats the honey and falls asleep.",
    "Bees sting me",
    "First I need an empty container.",
    "The bees all suffocated and disappeared",
    "Something I'm holding vibrates and...",
    "nothing to light it with",
    "Gas bladder blew up",
    "in my hands!",
    "gas needs to be contained before it will burn",
    "Gas dissipates. (I think you blew it)",
    "That won't ignite",
    "How?",
    "Bear won't let me",
    "\"Don't waste honey, get mad instead! Dam lava!?\"",
    "Bees madden bear, bear then attacks me!",
    "It soaks into the ground",
    "In 2 words tell me at what...like: AT TREE",
    "OH NO... Bear dodges... CRASH!",
    "Its heavy!",
    "Somethings too heavy. I fall.",
    "To stop game say QUIT",
    "Mirror hits floor and shatters into a MILLION pieces",
    "Mirror lands softly on rug, lights up and says:",
    "You lost *ALL* treasures.",
    "I'm not carrying ax, take inventory!",
    "It doesn't seem to bother him at all!",
    "The mud dried up and fell off.",
    "Bear is so startled that he FELL off the ledge!",
    "\" DRAGON STING \" and fades. I don't get it, I hope you do.",
    "The bees attack the dragon which gets so annoyed it gets up\nand flys away...",
    "Lamp is now full & lit",
    "\nI'm bitten by chiggers.\n",
    "There's something there all right! Maybe I should go there?",
    "Maybe if I threw something?...",
    "Too dry, the fish died.",
    "A glowing Genie appears, drops somehting, then vanishes.",
    "A glowing Genie appears, says \"Boy you're selfish\", takes\nsomething and then makes \"ME\" vanish!",
    "No, its too hot.",
    "Not here.",
    "Try the swamp",
    "Sizzle...",
    "Try --> \"LOOK, JUMP, SWIM, CLIMB, FIND, TAKE, SCORE, DROP\"\nand any other verbs you can think of...",
    "There are only 3 ways to wake the Dragon!",
    "Remember you can always say \"HELP\"",
    "Read the sign in the meadow!",
    "You may need to say magic words here",
    "A voice BOOOOMS out:",
    "please leave it alone",
    "Sorry, I can only throw the ax.",
    "Medicine is good for bites.",
    "I don't know where it is",
    "\nWelcome to Adventure number: 1 \"ADVENTURELAND\".\nIn this Adventure you're to find *TREASURES* & store them away.\n\nTo see how well you're doing say: \"SCORE\"",
    "Blow it up!",
    "Fish have escaped back to the lake.",
    "OK",
    "Huh? I don't think so!",
    "You might try examining things...",
    "What?",
    "OK, I threw it.",
    "\nCheck with your favorite computer dealer for the next Adventure\nprogram: PIRATE ADVENTURE. If they don't carry \"ADVENTURE\" have\nthem call: 1-305-862-6917 today!\n",
    "The ax vibrated!",
    "I see nothing special",
    "All treasures collected.",
};
const GameState InitState = {
	0, 0, PLAYER_START, LIGHT_REFILL, 0, 0, 0, {0}, {0},
	{
		0,
		4,
		4,
		2,
		0,
		1,
		10,
		1,
		10,
		0,
		3,
		10,
		3,
		0,
		2,
		3,
		5,
		0,
		1,
		18,
		0,
		0,
		1,
		8,
		8,
		21,
		0,
		23,
		30,
		17,
		18,
		0,
		20,
		23,
		18,
		0,
		0,
		22,
		21,
		0,
		9,
		0,
		1,
		0,
		0,
		0,
		25,
		26,
		0,
		0,
		14,
		33,
		0,
		10,
		17,
		0,
		0,
		25,
		11,
		12,
		0,
		0,
		29,
		29,
		0,
		0,
	}
};
#else
extern Item Items[];
extern char* Verbs[];
extern char* Nouns[];
extern Room Rooms[];
extern Action Actions[];
extern char* Messages[];
extern const GameState InitState;
#endif
