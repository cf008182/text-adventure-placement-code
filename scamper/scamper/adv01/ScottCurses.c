/*
 *	ScottFree Revision 1.14
 *
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 *
 *
 *	You must have an ANSI C compiler to build this program.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
// Curses and signals includes removed. - MML
// Size type include added. - MML
#include <stdint.h>
// Printing of fixed width types. - MML
#include <inttypes.h>

#include "Scott.h"

// Configuration for weird platforms removed. - MML

#define INTERPRETER
#include "db.h"

// GameTail removed, as it's never used. - MML
char NounText[16];

// The following moved to GameState: - MML
// int Counters[16];	/* Range unknown */
// int CurrentCounter;
// int SavedRoom;
// int RoomSaved[16];	/* Range unknown */

int Redraw;		/* Update item window */
int Options;		/* Option flags set */
int Width;		/* Terminal width */
// Curses variables removed. - MML

#define TRS80_LINE	"\n<------------------------------------------------------------>\n"

#define MyLoc	(Q.PlayerRoom)

// Moved to GameState. - MML
// long BitFlags=0;	/* Might be >32 flags - I haven't seen >32 yet */

GameState Q;

void Fatal(char *x)
{
	// Curses code removed. - MML
	fprintf(stderr,"%s.\n",x);
	exit(1);
}

void Aborted()
{
	Fatal("User exit");
}

void ClearScreen(void)
{
	// Curses code removed. - MML
}

void *MemAlloc(int size)
{
	void *t=(void *)malloc(size);
	if(t==NULL)
		Fatal("Out of memory");
	return(t);
}

int RandomPercent(int n)
{
	// Simplify random number generator for consistency. - MML
	Q.RandState = (Q.RandState + 49) % 100;
	if(Q.RandState<n)
		return(1);
	return(0);
}

int CountCarried()
{
	int ct=0;
	int n=0;
	while(ct<=MAX_ITEM)
	{
		if(Q.ItemLocs[ct]==CARRIED)
			n++;
		ct++;
	}
	return(n);
}

char *MapSynonym(char *word)
{
	int n=1;
	char *tp;
	static char lastword[16];	/* Last non synonym */
	while(n<=MAX_WORD)
	{
		tp=Nouns[n];
		if(*tp=='*')
			tp++;
		else
			strcpy(lastword,tp);
		if(strncasecmp(word,tp,WORD_LENGTH)==0)
			return(lastword);
		n++;
	}
	return(NULL);
}

int MatchUpItem(char *text, int loc)
{
	char *word=MapSynonym(text);
	int ct=0;
	
	if(word==NULL)
		word=text;
	
	while(ct<=MAX_ITEM)
	{
		if(Items[ct].AutoGet && Q.ItemLocs[ct]==loc &&
			strncasecmp(Items[ct].AutoGet,word,WORD_LENGTH)==0)
			return(ct);
		ct++;
	}
	return(-1);
}

char *ReadString(FILE *f)
{
	char tmp[1024];
	char *t;
	int c,nc;
	int ct=0;
	do
	{
		c=fgetc(f);
	}
	while(c!=EOF && isspace(c));
	if(c!='"')
	{
		Fatal("Initial quote expected");
	}
	do
	{
		c=fgetc(f);
		if(c==EOF)
			Fatal("EOF in string");
		if(c=='"')
		{
			nc=fgetc(f);
			if(nc!='"')
			{
				ungetc(nc,f);
				break;
			}
		}
		if(c==0x60) 
			c='"'; /* pdd */
		tmp[ct++]=c;
	}
	while(1);
	tmp[ct]=0;
	t=MemAlloc(ct+1);
	memcpy(t,tmp,ct+1);
	return(t);
}
	
void LoadDatabase(FILE *f, int loud)
{


}

int OutputPos=0;

void OutReset()
{
	OutputPos=0;
	// Curses code removed. - MML
}

void OutBuf(char *buffer)
{
	char word[80];
	int wp;
	while(*buffer)
	{
		if(OutputPos==0)
		{
			while(*buffer && isspace(*buffer))
			{
				if(*buffer=='\n')
				{
					// Curses code removed. - MML
					printf("\n");
					OutputPos=0;
				}
				buffer++;
			}
		}
		if(*buffer==0)
			return;
		wp=0;
		while(*buffer && !isspace(*buffer))
		{
			word[wp++]=*buffer++;
		}
		word[wp]=0;
/*		fprintf(stderr,"Word '%s' at %d\n",word,OutputPos);*/
		if(OutputPos+strlen(word)>(Width-2))
		{
			// Curses code removed. - MML
			printf("\n");
			OutputPos=0;
		}
		// Curses code removed. - MML
		fputs(word, stdout);
		OutputPos+=strlen(word);
		
		if(*buffer==0)
			return;
		
		if(*buffer=='\n')
		{
			// Curses code removed. - MML
			printf("\n");
			OutputPos=0;
		}
		else
		{
			OutputPos++;
			if(OutputPos<(Width-1)) {
				// Curses code removed. - MML
				printf(" ");
			}
		}
		buffer++;
	}
}

void Output(char *a)
{
	char block[512];
	strcpy(block,a);
	OutBuf(block);
}

void OutputNumber(int a)
{
	char buf[16];
	sprintf(buf,"%d ",a);
	OutBuf(buf);
}
		
void Look()
{
	// Curses code removed/modified throughout this function.
	// Replaced with printf to make stdin/stdout redirection easier.
	// - MML
	static char *ExitNames[6]=
	{
		"North","South","East","West","Up","Down"
	};
	Room *r;
	int ct,f;
	int pos;
	
	printf("\n");

	if((Q.BitFlags&(1<<DARKBIT)) && Q.ItemLocs[LIGHT_SOURCE]!= CARRIED
	            && Q.ItemLocs[LIGHT_SOURCE]!= MyLoc)
	{
		if(Options&YOUARE)
			printf("You can't see. It is too dark!\n");
		else
			printf("I can't see. It is too dark!\n");
		if (Options & TRS80_STYLE)
			printf(TRS80_LINE);
		return;
	}
	r=&Rooms[MyLoc];
	if(*r->Text=='*')
		printf("%s\n",r->Text+1);
	else
	{
		if(Options&YOUARE)
			printf("You are %s\n",r->Text);
		else
			printf("I'm in a %s\n",r->Text);
	}
	ct=0;
	f=0;
	printf("\nObvious exits: ");
	while(ct<6)
	{
		if(r->Exits[ct]!=0)
		{
			if(f==0)
				f=1;
			else
				printf(", ");
			printf("%s",ExitNames[ct]);
		}
		ct++;
	}
	if(f==0)
		printf("none");
	printf(".\n");
	ct=0;
	f=0;
	pos=0;
	while(ct<=MAX_ITEM)
	{
		if(Q.ItemLocs[ct]==MyLoc)
		{
			if(f==0)
			{
				if(Options&YOUARE)
					printf("\nYou can also see: ");
				else
					printf("\nI can also see: ");
				pos=16;
				f++;
			}
			else if (!(Options & TRS80_STYLE))
			{
				printf(" - ");
				pos+=3;
			}
			if(pos+strlen(Items[ct].Text)>(Width-10))
			{
				pos=0;
				printf("\n");
			}
			printf("%s",Items[ct].Text);
			pos += strlen(Items[ct].Text);
			if (Options & TRS80_STYLE)
			{
				printf(". ");
				pos+=2;
			}
		}
		ct++;
	}
	printf("\n");
	if (Options & TRS80_STYLE)
		printf(TRS80_LINE);
}

int WhichWord(char *word, char **list)
{
	int n=1;
	int ne=1;
	char *tp;
	while(ne<=MAX_WORD)
	{
		tp=list[ne];
		if(*tp=='*')
			tp++;
		else
			n=ne;
		if(strncasecmp(word,tp,WORD_LENGTH)==0)
			return(n);
		ne++;
	}
	return(-1);
}



// LineInput function removed. Uses replaced with fgets(). - MML

void GetInput(vb,no)
int *vb,*no;
{
	char buf[256];
	char verb[10],noun[10];
	int vc,nc;
	int num;
	do
	{
		do
		{
			Output("\nTell me what to do ? ");
			// Curses code removed. - MML
			if(fgets(buf, 256, stdin) == NULL) {
				//strcpy(buf, "quit\n");
				exit(0);
			}
			// Echo input for easy creation of logs from input scripts.
			printf("\n> %s", buf);
			OutReset();
			num=sscanf(buf,"%9s %9s",verb,noun);
		}
		while(num==0||*buf=='\n');
		if(num==1)
			*noun=0;
		if(*noun==0 && strlen(verb)==1)
		{
			switch(isupper(*verb)?tolower(*verb):*verb)
			{
				case 'n':strcpy(verb,"NORTH");break;
				case 'e':strcpy(verb,"EAST");break;
				case 's':strcpy(verb,"SOUTH");break;
				case 'w':strcpy(verb,"WEST");break;
				case 'u':strcpy(verb,"UP");break;
				case 'd':strcpy(verb,"DOWN");break;
				/* Brian Howarth interpreter also supports this */
				case 'i':strcpy(verb,"INVENTORY");break;
			}
		}
		nc=WhichWord(verb,Nouns);
		/* The Scott Adams system has a hack to avoid typing 'go' */
		if(nc>=1 && nc <=6)
		{
			vc=1;
		}
		else
		{
			vc=WhichWord(verb,Verbs);
			nc=WhichWord(noun,Nouns);
		}
		*vb=vc;
		*no=nc;
		if(vc==-1)
		{
			Output("You use word(s) I don't know! ");
		}
	}
	while(vc==-1);
	strcpy(NounText,noun);	/* Needed by GET/DROP hack */
}

void SaveGame()
{
	char buf[256];
	int ct;
	FILE *f;
	Output("Filename: ");
	fgets(buf, 256, stdin);
	Output("\n");
	f=fopen(buf,"w");
	if(f==NULL)
	{
		Output("Unable to create save file.\n");
		return;
	}
	for(ct=0;ct<16;ct++)
	{
		fprintf(f,"%" PRId8 " %" PRId8 "\n",Q.Counters[ct],Q.RoomSaved[ct]);
	}
	fprintf(f,"%" PRIu32 " %" PRIu8 " %" PRIu8 " %" PRId8 " %" PRIu8 " %" PRIu8 "\n",Q.BitFlags, (Q.BitFlags&(1<<DARKBIT))?1:0,
		MyLoc,Q.CurrentCounter,Q.SavedRoom,Q.LightTime);
	for(ct=0;ct<=MAX_ITEM;ct++)
		fprintf(f,"%hd\n",(short)Q.ItemLocs[ct]);
	fclose(f);
	Output("Saved.\n");
}

void LoadGame(char *name)
{
	FILE *f=fopen(name,"r");
	int ct=0;
	short lo;
	short DarkFlag;
	if(f==NULL)
	{
		Output("Unable to restore game.");
		return;
	}
	for(ct=0;ct<16;ct++)
	{
		fscanf(f,"%" SCNd8 " %" SCNu8 "\n",&Q.Counters[ct],&Q.RoomSaved[ct]);
	}
	fscanf(f,"%" SCNu32 " %hd" " %" SCNu8 " %" SCNd8 " %" SCNu8 " %" SCNu8 "\n",
		&Q.BitFlags,&DarkFlag,&MyLoc,&Q.CurrentCounter,&Q.SavedRoom,
		&Q.LightTime);
	/* Backward compatibility */
	if(DarkFlag)
		Q.BitFlags|=(1<<15);
	for(ct=0;ct<=MAX_ITEM;ct++)
	{
		fscanf(f,"%hd\n",&lo);
		Q.ItemLocs[ct]=(unsigned char)lo;
	}
	fclose(f);
}

int PerformLine(int ct)
{
	int continuation=0;
	int param[5],pptr=0;
	int act[4];
	int cc=0;
	while(cc<5)
	{
		int cv,dv;
		cv=Actions[ct].Condition[cc];
		dv=cv/20;
		cv%=20;
		switch(cv)
		{
			case 0:
				param[pptr++]=dv;
				break;
			case 1:
				if(Q.ItemLocs[dv]!=CARRIED)
					return(0);
				break;
			case 2:
				if(Q.ItemLocs[dv]!=MyLoc)
					return(0);
				break;
			case 3:
				if(Q.ItemLocs[dv]!=CARRIED&&
					Q.ItemLocs[dv]!=MyLoc)
					return(0);
				break;
			case 4:
				if(MyLoc!=dv)
					return(0);
				break;
			case 5:
				if(Q.ItemLocs[dv]==MyLoc)
					return(0);
				break;
			case 6:
				if(Q.ItemLocs[dv]==CARRIED)
					return(0);
				break;
			case 7:
				if(MyLoc==dv)
					return(0);
				break;
			case 8:
				if((Q.BitFlags&(1<<dv))==0)
					return(0);
				break;
			case 9:
				if(Q.BitFlags&(1<<dv))
					return(0);
				break;
			case 10:
				if(CountCarried()==0)
					return(0);
				break;
			case 11:
				if(CountCarried())
					return(0);
				break;
			case 12:
				if(Q.ItemLocs[dv]==CARRIED||Q.ItemLocs[dv]==MyLoc)
					return(0);
				break;
			case 13:
				if(Q.ItemLocs[dv]==0)
					return(0);
				break;
			case 14:
				if(Q.ItemLocs[dv])
					return(0);
				break;
			case 15:
				if(Q.CurrentCounter>dv)
					return(0);
				break;
			case 16:
				if(Q.CurrentCounter<=dv)
					return(0);
				break;
			case 17:
				if(Q.ItemLocs[dv]!=Items[dv].InitialLoc)
					return(0);
				break;
			case 18:
				if(Q.ItemLocs[dv]==Items[dv].InitialLoc)
					return(0);
				break;
			case 19:/* Only seen in Brian Howarth games so far */
				if(Q.CurrentCounter!=dv)
					return(0);
				break;
		}
		cc++;
	}
	/* Actions */
	act[0]=Actions[ct].Action[0];
	act[2]=Actions[ct].Action[1];
	act[1]=act[0]%150;
	act[3]=act[2]%150;
	act[0]/=150;
	act[2]/=150;
	cc=0;
	pptr=0;
	while(cc<4)
	{
		if(act[cc]>=1 && act[cc]<52)
		{
			Output(Messages[act[cc]]);
			Output("\n");
		}
		else if(act[cc]>101)
		{
			Output(Messages[act[cc]-50]);
			Output("\n");
		}
		else switch(act[cc])
		{
			case 0:/* NOP */
				break;
			case 52:
				if(CountCarried()==MAX_CARRY)
				{
					if(Options&YOUARE)
						Output("You are carrying too much. ");
					else
						Output("I've too much to carry! ");
					break;
				}
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]= CARRIED;
				break;
			case 53:
				Redraw=1;
				Q.ItemLocs[param[pptr++]]=MyLoc;
				break;
			case 54:
				Redraw=1;
				MyLoc=param[pptr++];
				break;
			case 55:
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]=0;
				break;
			case 56:
				Q.BitFlags|=1<<DARKBIT;
				break;
			case 57:
				Q.BitFlags&=~(1<<DARKBIT);
				break;
			case 58:
				Q.BitFlags|=(1<<param[pptr++]);
				break;
			case 59:
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]=0;
				break;
			case 60:
				Q.BitFlags&=~(1<<param[pptr++]);
				break;
			case 61:
				if(Options&YOUARE)
					Output("You are dead.\n");
				else
					Output("I am dead.\n");
				Q.BitFlags&=~(1<<DARKBIT);
				MyLoc=MAX_ROOM;/* It seems to be what the code says! */
				Look();
				break;
			case 62:
			{
				/* Bug fix for some systems - before it could get parameters wrong */
				int i=param[pptr++];
				Q.ItemLocs[i]=param[pptr++];
				Redraw=1;
				break;
			}
			case 63:
doneit:				Output("The game is now over.\n");
				// Curses code removed. - MML
				exit(0);
			case 64:
				Look();
				break;
			case 65:
			{
				int ct=0;
				int n=0;
				while(ct<=MAX_ITEM)
				{
					if(Q.ItemLocs[ct]==TREASURE_ROOM &&
					  *Items[ct].Text=='*')
					  	n++;
					ct++;
				}
				if(Options&YOUARE)
					Output("You have stored ");
				else
					Output("I've stored ");
				OutputNumber(n);
				Output(" treasures.  On a scale of 0 to 100, that rates ");
				OutputNumber((n*100)/TREASURES);
				Output(".\n");
				if(n==TREASURES)
				{
					Output("Well done.\n");
					goto doneit;
				}
				break;
			}
			case 66:
			{
				int ct=0;
				int f=0;
				if(Options&YOUARE)
					Output("You are carrying:\n");
				else
					Output("I'm carrying:\n");
				while(ct<=MAX_ITEM)
				{
					if(Q.ItemLocs[ct]==CARRIED)
					{
						if(f==1)
						{
							if (Options & TRS80_STYLE)
								Output(". ");
							else
								Output(" - ");
						}
						f=1;
						Output(Items[ct].Text);
					}
					ct++;
				}
				if(f==0)
					Output("Nothing");
				Output(".\n");
				break;
			}
			case 67:
				Q.BitFlags|=(1<<0);
				break;
			case 68:
				Q.BitFlags&=~(1<<0);
				break;
			case 69:
				Q.LightTime=LIGHT_REFILL;
				if(Q.ItemLocs[LIGHT_SOURCE]==MyLoc)
					Redraw=1;
				Q.ItemLocs[LIGHT_SOURCE]=CARRIED;
				Q.BitFlags&=~(1<<LIGHTOUTBIT);
				break;
			case 70:
				ClearScreen(); /* pdd. */
				OutReset();
				break;
			case 71:
				SaveGame();
				break;
			case 72:
			{
				int i1=param[pptr++];
				int i2=param[pptr++];
				int t=Q.ItemLocs[i1];
				if(t==MyLoc || Q.ItemLocs[i2]==MyLoc)
					Redraw=1;
				Q.ItemLocs[i1]=Q.ItemLocs[i2];
				Q.ItemLocs[i2]=t;
				break;
			}
			case 73:
				continuation=1;
				break;
			case 74:
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]= CARRIED;
				break;
			case 75:
			{
				int i1,i2;
				i1=param[pptr++];
				i2=param[pptr++];
				if(Q.ItemLocs[i1]==MyLoc)
					Redraw=1;
				Q.ItemLocs[i1]=Q.ItemLocs[i2];
				if(Q.ItemLocs[i2]==MyLoc)
					Redraw=1;
				break;
			}
			case 76:	/* Looking at adventure .. */
				Look();
				break;
			case 77:
				if(Q.CurrentCounter>=0)
					Q.CurrentCounter--;
				break;
			case 78:
				OutputNumber(Q.CurrentCounter);
				break;
			case 79:
				Q.CurrentCounter=param[pptr++];
				break;
			case 80:
			{
				int t=MyLoc;
				MyLoc=Q.SavedRoom;
				Q.SavedRoom=t;
				Redraw=1;
				break;
			}
			case 81:
			{
				/* This is somewhat guessed. Claymorgue always
				   seems to do select counter n, thing, select counter n,
				   but uses one value that always seems to exist. Trying
				   a few options I found this gave sane results on ageing */
				int t=param[pptr++];
				int c1=Q.CurrentCounter;
				Q.CurrentCounter=Q.Counters[t];
				Q.Counters[t]=c1;
				break;
			}
			case 82:
				Q.CurrentCounter+=param[pptr++];
				break;
			case 83:
				Q.CurrentCounter-=param[pptr++];
				if(Q.CurrentCounter< -1)
					Q.CurrentCounter= -1;
				/* Note: This seems to be needed. I don't yet
				   know if there is a maximum value to limit too */
				break;
			case 84:
				Output(NounText);
				break;
			case 85:
				Output(NounText);
				Output("\n");
				break;
			case 86:
				Output("\n");
				break;
			case 87:
			{
				/* Changed this to swap location<->roomflag[x]
				   not roomflag 0 and x */
				int p=param[pptr++];
				int sr=MyLoc;
				MyLoc=Q.RoomSaved[p];
				Q.RoomSaved[p]=sr;
				Redraw=1;
				break;
			}
			case 88:
				// Curses code removed. - MML
				break;
			case 89:
				pptr++;
				/* SAGA draw picture n */
				/* Spectrum Seas of Blood - start combat ? */
				/* Poking this into older spectrum games causes a crash */
				break;
			default:
				fprintf(stderr,"Unknown action %d [Param begins %d %d]\n",
					act[cc],param[pptr],param[pptr+1]);
				break;
		}
		cc++;
	}
	return(1+continuation);		
}


int PerformActions(int vb,int no)
{
	static int disable_sysfunc=0;	/* Recursion lock */
	int d=Q.BitFlags&(1<<DARKBIT);
	
	int ct=0;
	int fl;
	int doagain=0;
	if(vb==1 && no == -1 )
	{
		Output("Give me a direction too.");
		return(0);
	}
	if(vb==1 && no>=1 && no<=6)
	{
		int nl;
		if(Q.ItemLocs[LIGHT_SOURCE]==MyLoc ||
		   Q.ItemLocs[LIGHT_SOURCE]==CARRIED)
		   	d=0;
		if(d)
			Output("Dangerous to move in the dark! ");
		nl=Rooms[MyLoc].Exits[no-1];
		if(nl!=0)
		{
			MyLoc=nl;
			Look();
			return(0);
		}
		if(d)
		{
			if(Options&YOUARE)
				Output("You fell down and broke your neck. ");
			else
				Output("I fell down and broke my neck. ");
			// Curses code removed. - MML
			exit(0);
		}
		if(Options&YOUARE)
			Output("You can't go in that direction. ");
		else
			Output("I can't go in that direction. ");
		return(0);
	}
	fl= -1;
	while(ct<=MAX_ACTION)
	{
		int vv,nv;
		vv=Actions[ct].Vocab;
		/* Think this is now right. If a line we run has an action73
		   run all following lines with vocab of 0,0 */
		if(vb!=0 && (doagain&&vv!=0))
			break;
		/* Oops.. added this minor cockup fix 1.11 */
		if(vb!=0 && !doagain && fl== 0)
			break;
		nv=vv%150;
		vv/=150;
		if((vv==vb)||(doagain&&Actions[ct].Vocab==0))
		{
			if((vv==0 && RandomPercent(nv))||doagain||
				(vv!=0 && (nv==no||nv==0)))
			{
				int f2;
				if(fl== -1)
					fl= -2;
				if((f2=PerformLine(ct))>0)
				{
					/* ahah finally figured it out ! */
					fl=0;
					if(f2==2)
						doagain=1;
					if(vb!=0 && doagain==0)
						return 0;
				}
			}
		}
		ct++;
		if(ct<=MAX_ACTION && Actions[ct].Vocab!=0)
			doagain=0;
	}
	if(fl!=0 && disable_sysfunc==0)
	{
		int i;
		if(Q.ItemLocs[LIGHT_SOURCE]==MyLoc ||
		   Q.ItemLocs[LIGHT_SOURCE]==CARRIED)
		   	d=0;
		if(vb==10 || vb==18)
		{
			/* Yes they really _are_ hardcoded values */
			if(vb==10)
			{
				if(strcasecmp(NounText,"ALL")==0)
				{
					int ct=0;
					int f=0;
					
					if(d)
					{
						Output("It is dark.\n");
						return 0;
					}
					while(ct<=MAX_ITEM)
					{
						if(Q.ItemLocs[ct]==MyLoc && Items[ct].AutoGet!=NULL && Items[ct].AutoGet[0]!='*')
						{
							no=WhichWord(Items[ct].AutoGet,Nouns);
							disable_sysfunc=1;	/* Don't recurse into auto get ! */
							PerformActions(vb,no);	/* Recursively check each items table code */
							disable_sysfunc=0;
							if(CountCarried()==MAX_CARRY)
							{
								if(Options&YOUARE)
									Output("You are carrying too much. ");
								else
									Output("I've too much to carry. ");
								return(0);
							}
						 	Q.ItemLocs[ct]= CARRIED;
						 	Redraw=1;
						 	OutBuf(Items[ct].Text);
						 	Output(": O.K.\n");
						 	f=1;
						 }
						 ct++;
					}
					if(f==0)
						Output("Nothing taken.");
					return(0);
				}
				if(no==-1)
				{
					Output("What ? ");
					return(0);
				}
				if(CountCarried()==MAX_CARRY)
				{
					if(Options&YOUARE)
						Output("You are carrying too much. ");
					else
						Output("I've too much to carry. ");
					return(0);
				}
				i=MatchUpItem(NounText,MyLoc);
				if(i==-1)
				{
					if(Options&YOUARE)
						Output("It is beyond your power to do that. ");
					else
						Output("It's beyond my power to do that. ");
					return(0);
				}
				Q.ItemLocs[i]= CARRIED;
				Output("O.K. ");
				Redraw=1;
				return(0);
			}
			if(vb==18)
			{
				if(strcasecmp(NounText,"ALL")==0)
				{
					int ct=0;
					int f=0;
					while(ct<=MAX_ITEM)
					{
						if(Q.ItemLocs[ct]==CARRIED && Items[ct].AutoGet && Items[ct].AutoGet[0]!='*')
						{
							no=WhichWord(Items[ct].AutoGet,Nouns);
							disable_sysfunc=1;
							PerformActions(vb,no);
							disable_sysfunc=0;
							Q.ItemLocs[ct]=MyLoc;
							OutBuf(Items[ct].Text);
							Output(": O.K.\n");
							Redraw=1;
							f=1;
						}
						ct++;
					}
					if(f==0)
						Output("Nothing dropped.\n");
					return(0);
				}
				if(no==-1)
				{
					Output("What ? ");
					return(0);
				}
				i=MatchUpItem(NounText,CARRIED);
				if(i==-1)
				{
					if(Options&YOUARE)
						Output("It's beyond your power to do that.\n");
					else
						Output("It's beyond my power to do that.\n");
					return(0);
				}
				Q.ItemLocs[i]=MyLoc;
				Output("O.K. ");
				Redraw=1;
				return(0);
			}
		}
	}
	return(fl);
}
	
int main(int argc, char *argv[])
{
	int vb,no;
	
	while(argv[1])
	{
		if(*argv[1]!='-')
			break;
		switch(argv[1][1])
		{
			case 'y':
				Options|=YOUARE;
				break;
			case 'i':
				Options&=~YOUARE;
				break;
			case 'd':
				Options|=DEBUGGING;
				break;
			case 's':
				Options|=SCOTTLIGHT;
				break;
			case 't':
				Options|=TRS80_STYLE;
				break;
			case 'p':
				Options|=PREHISTORIC_LAMP;
				break;
			case 'h':
			default:
				fprintf(stderr,"%s: [-h] [-y] [-s] [-i] [-t] [-d] [-p] <gamename> [savedgame].\n",
						argv[0]);
				exit(1);
		}
		if(argv[1][2]!=0)
		{
			fprintf(stderr,"%s: option -%c does not take a parameter.\n",
				argv[0],argv[1][1]);
			exit(1);
		}
		argv++;
		argc--;
	}			

	if(argc!=1 && argc!=2)
	{
		fprintf(stderr,"%s <savefile>.\n",argv[0]);
		exit(1);
	}
	// Loading game database from file removed. - MML

	// BSD-specific signals removed. - MML
	
	if (Options & TRS80_STYLE)
	{
		Width = 64;
	}
	else
	{
		Width = 80;
	}

	// Curses code removed. - MML
	OutReset();
	OutBuf("\
Scott Free, A Scott Adams game driver in C.\n\
Release 1.14, (c) 1993,1994,1995 Swansea University Computer Society.\n\
Distributed under the GNU software license\n\n");
	// No need to load the database, as it's now hard-coded.
	// But the game state needs to be initialised to the starting state. - MML
	Q = InitState;
	if(argc==2)
		LoadGame(argv[1]);
	Look();
	while(1)
	{
		if(Redraw!=0)
		{
			Look();
			Redraw=0;
		}
		PerformActions(0,0);
		if(Redraw!=0)
		{
			Look();
			Redraw=0;
		}
		GetInput(&vb,&no);
		switch(PerformActions(vb,no))
		{
			case -1:Output("I don't understand your command. ");
				break;
			case -2:Output("I can't do that yet. ");
				break;
		}
		/* Brian Howarth games seem to use -1 for forever */
		if(Q.ItemLocs[LIGHT_SOURCE]/*==-1*/!=DESTROYED && Q.LightTime!= INF_LIGHT)
		{
			Q.LightTime--;
			if(Q.LightTime<1)
			{
				Q.BitFlags|=(1<<LIGHTOUTBIT);
				if(Q.ItemLocs[LIGHT_SOURCE]==CARRIED ||
					Q.ItemLocs[LIGHT_SOURCE]==MyLoc)
				{
					if(Options&SCOTTLIGHT)
						Output("Light has run out! ");
					else
						Output("Your light has run out. ");
				}
				if(Options&PREHISTORIC_LAMP)
					Q.ItemLocs[LIGHT_SOURCE]=DESTROYED;
			}
			else if(Q.LightTime<25)
			{
				if(Q.ItemLocs[LIGHT_SOURCE]==CARRIED ||
					Q.ItemLocs[LIGHT_SOURCE]==MyLoc)
				{
			
					if(Options&SCOTTLIGHT)
					{
						Output("Light runs out in ");
						OutputNumber(Q.LightTime);
						Output(" turns. ");
					}
					else
					{
						if(Q.LightTime%5==0)
							Output("Your light is growing dim. ");
					}
				}
			}
		}
	}
}
