#!/bin/sh
echo 'Game, Max item, Max action, Max word, Max room, Action triggers, MC/DC condition permutations, Gettable items' > sizestats.csv
grep GAMESIZE: */log.txt >> sizestats.csv

echo 'Game, Rooms reached, out of, percent, Messages reached, out of, percent, Actions reached, out of, percent,' \
  ' MC/DC conditions reached, out of, percent, States explored, Time (s), Memory (MB)' > searchstats.csv
ls */log.txt | xargs -n 1 -I X sh -c 'echo -n X, && tail -n 1 X' >> searchstats.csv

