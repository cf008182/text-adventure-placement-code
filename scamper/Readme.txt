ScAmPER (Scott Adams exPlicitly Evaluating Reachability) 0.3
============================================================

Overview
--------

ScAmPER analyses Scott Adams format interactive fiction games using a
heuristic-guided search.

Specifically, it searches for reachable scripted actions and displayed
messages within a game. When it finds them, it outputs the sequence of
in-game commands needed to reach them. It also produces an animated
visualisation of the search process.

Contents of Archive
-------------------

For users:

License.txt                  Software licensing information.
Readme.txt                   This file.
scamper/packages/            Ubuntu packages for dot/gifsicle.
scamper/scamper.sh           Script to run the tool.
scamper/games/sac/           Games from Mike Taylor's Scott Adams Compiler.
scamper/games/saga/          Original Scott Adams Grand Adventure games.
scamper/sizestats-all.csv    Stats about the size of the games.
scamper/searchstats-*.csv    Benchmark results for the games.
scamper/benchmark.sh         Run tool on all the games.
scamper/gather-stats.sh      After running tool, create stats CSV files.

Used by ScAmPER:

scamper/dumper/              From a game, creates C header file that
                             hard-codes the game's database.
scamper/scamper/             The tool itself. Searches a game for
                             interesting states and dumps test cases and
                             visualisation of the search.
scamper/visualise.sh         Script to generate animated GIF from frames of
                             the visualisation.
scamper/logger/              Game interpreter with database hard-coded that
                             uses standard input/output.
scamper/log-paths.sh         Script to run logger on all the test cases and
                             record the output.
scamper/scottfree            Original C implementation of Scott Adams
                             interpreter, used as the basis for ScAmPER.

Usage
-----

You need a C++ compiler (probably supporting C++17 or later) and, for
visualisation generation, GraphViz's dot and gifsicle.

Packages for dot and gifsicle are included in the archive. To install, run:

> sudo dpkg --install scamper/packages/*.deb

As described in the paper, the tool is actually recompiled for every
different game. This hard-codes the database of the game inside the tool,
which allows for faster, optimised evaluation of the "next state" function.
The SPIN model checker uses a similar strategy.

To run the tool, from the "scamper" directory, run:

> ./scamper.sh <game.dat>

This does the following for you:

1. Copy the subdirectory scamper to a subdirectory <game>/ to hold the tool.
2. Create subdirectories <game>/maps/ and <game>/paths/ to hold output.
2. Compile dumper, if you haven't already done so.
3. Run dumper on the game.dat you supplied, generating <game>/db.h.
4. Compile <game>/scamper and <game>/logger .
5. Run <game>/scamper from the <game>/ subdirectory, analysing the game and
generating output in <game>/maps/ and <game>/paths/ .
6. Run dot on the contents of <game>/maps/ to generate GIFs from the maps
and gifsicle on the results to generate an animated visualisation,
<game>/maps/map.gif .
7. Run logger on the contents of <game>/paths/ to generate transcripts from
the commands.

You can view <game>/map.gif in a web browser:

> firefox <game>/map.gif

The files in <game>/paths/ are plain text.

If you actually want to play the game, <game>/logger can be used for this
purpose interactively, although you might be better off downloading ScottFree
(Debian package scottfree) and running it on the .dat directly.

Reproducing Results from Paper
------------------------------

To reproduce the image from the paper, run "./scamper.sh games/sac/t4.dat". The
image in the paper was frame number 6 in t4/maps/ , although I moved one of
the grids manually to make it fit nicely on the printed page.

To check the figures in the table for the first 5 classic SAGA games, run:

> ./scamper.sh games/saga/adv01.dat
> ./scamper.sh games/saga/adv02.dat
> ./scamper.sh games/saga/adv03.dat
> ./scamper.sh games/saga/adv04.dat
> ./scamper.sh games/saga/adv05.dat

Look at the log output on-screen. Or, if you let the tool terminate, run
"./gather-stats.sh" and look at sizestats.csv and searchstats.csv.

You can even run "./benchmark.sh", which will run the tool on all the games
for you. For ScAmPER 0.3, benchmark.sh took about 2 hours and needed 2 GB of
RAM on my machine.

To run the benchmarks, I used a Dell XPS 13 9360, which has an Intel Core
i7-7500U processor with 16 GB of RAM, and was running Debian Linux 9.
The files "searchstats-all-*.csv" and "sizestats-all.csv" show stats from
running benchmark.sh on my machine.

This archive does not include code from my preliminary work, which showed
that various other tools were unsuccessful.

Commentary on Results
---------------------

60-90% coverage, by whatever metric, is reasonable, although not superb. 
However, as noted in the paper, it's considerably better than I was able to
achieve using some of the top C tools in SV-COMP directly on the
interpreter.

Typically, the tool achieves most of the coverage in the first few minutes.
After that, it is relying mainly on random exploration. Periodically, it
will find a new permutation of conditions, as tracked for MC/DC coverage,
which sometimes leads to a spurt of targeted exploration and increases
coverage further.

Note that the tool is able to complete the small tutorial games (t3, t4 and
t5) from the Scott Adams Compiler. It is also able to explore these
exhaustively, although you need to turn off the usual termination condition
in the source code to explore all 1,285,247 states of t5.

In most cases, the majority of rooms, actions and messages in a game ought
to be reachable. However, almost all rooms contain a few dummy or
placeholder items that can never be reached during play. In particular, the
Adventureland Sampler, which is a cut-down version of Adventureland,
contains many rooms and actions from the full game, but without the lamp,
there is no way of reaching them.

The main weakness of the tool is that the heuristics are very crude and
don't handle a few common kinds of "puzzle" well. For example, many games
include dark areas that can't be explored without a lamp. I haven't
implemented a heuristic to prioritise finding the lamp before entering these
areas. Some games require you to collect a number of "treasure" items in a
specific "treasure room". I haven't implemented a heuristic to prioritise
leaving treasure in the treasure room.

There are also puzzles that require you to obtain a particular combination
of items before entering a certain room. One could instrument the
interpreter to detect that these items are needed at a certain state, then
backtrack up the game tree and attempt to obtain them before entering the
room.

On the other hand, the kinds of puzzle that were most frustrating to me as a
player of interactive fiction games were those that involved a bizarre or
unintuitive combination of objects, or fighting with the parser to guess the
correct verb to use to perform an action. These puzzles are solved very
easily by the tool, as it has access to the game's list of verbs/nouns and
can see what commands trigger events.

The list of useful heuristics is potentially quite long. I suspect that a
careful examination of why the tool fails to complete particular games would
allow one to produce a fairly short list that would be sufficient to
complete all the classic games. A more general approach would be to use some
constraint solver or first-order theorem prover, but that would be a
significant departure from the philosophy and architecture of the tool,
which is to try as many possibilities as quickly as possible in the hope
that good ones will be found.

Usefulness and Interest
-----------------------

What's the practical use of a tool that analyses games in a format that
almost no-one uses any more, and where all released games have published
solutions? Not much, but one could imagine that, if such games were still
being released, there could be 2 uses:

1. A player of the game could use it if stuck to find out how to progress.

2. A developer of a game could use it to check for unreachable or difficult
to reach parts of the game, which might indicate an error in development.

My real purpose for pursuing this is that (a) the application domain is
interesting and (b) I want to motivate the development of general tools to
handle this kind of problem.

Source Code
-----------

There are two main source code files you might care to inspect. One is the
modified version of ScottCurses.cpp in scamper. Compare that with
ScottCurses.c, as in the directory scottfree, to see what I have cut, what
I have reworked and what instrumentation I have added.

The other is Scamper.cpp, which implements the search itself and produces
the output.

Release
-------

The tool is available publicly on my personal webpage and Zenodo.

Changelog
---------

Version 0.3 (Mar 2020):
* Dependency on Boost for hashing removed.
* Code cleaned up slightly. All compiler warnings fixed or silenced.
* Array out-of-bounds bug in ScottFree code fixed. It caused crashes with
optimisations turned on.
* Compiler optimisations turned. Significant speed improvement.
* Switched to clang for default compiler, as slightly faster than gcc.
* Memory usage limit introduced: Stop if the process uses 8 GB of memory.
* Bugfix: CurrentCounter should have been signed. Coverage of some games
will change slightly as a result.

Version 0.2 (Feb 2020):
* MC/DC coverage added and used to identify novel states. Overall coverage
improved as a result.
* State hashing implemented to avoid duplicate states.
* Better logging and collection of statistics added.
* Table of statistics for bundled games included.
* Stagnation limit introduced: Stop after 100,000 states if explored state
space has doubled since last novel state.

Version 0.1 (Oct 2019):
* Initial release.

Todo
----

* Better search heuristics.
* Dump game script in human-readable form.
* Use basic static analysis to flag obviously unreachable parts of a game.
* Better layout of maps in visualisation?

