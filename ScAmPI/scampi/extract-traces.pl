#!/usr/bin/perl

use strict;

`grep 'Game over line .*: FAILURE\\|^Trace for\\|INPUT' logs/trace-log.txt > traces.txt`;
open(FH, "<", "traces.txt") or die $!;

my $line = -1;
my $verb = "";
my %fails = {};
while (my $x = <FH>) {
	if ($x =~ m/\[(.+)\] Game over line (\d+)/ ) {
		$fails{$1} = $2;
	}
	elsif ( $x =~ m/^Trace for (.+):/ ) {
		if ($line > -1) {
			close(TRACE) or die $!;
		}
		$line = $fails{$1};
		my $name = sprintf "line%03d.txt", $line;
		open(TRACE, ">", "paths/$name") or die $!;
	}
	elsif ( $x =~ m/INPUT verb: "(.*?)"/ ) {
		$verb = $1;
	}
	elsif ( $x =~ m/INPUT noun: "(.*?)"/ ) {
		print TRACE "$verb $1\n";
	}
	else {
		die "Unexpected line: $x\n";
	}
}

if ($line > -1) {
	close(TRACE) or die $!;
}

close(FH) or die $!;

