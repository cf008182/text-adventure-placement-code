#!/usr/bin/perl

use strict;

open STATS, ">", "stats.csv" or die $!;
print STATS "Game,Items,Actions,Rooms,Moves,CBMC CPU time (s),Of which solver time (s),Max res memory (MB)\n";

my @games = `find games -name "*.dat"`;
foreach my $game (@games) {
    chomp $game;
    my $short = `basename $game`;
    $short =~ s/.dat// ;
    chomp $short;
    
    if (!-e $short) {
        next;
    }
    
    my $items = `grep MAX_ITEM $short/db.h`;
    my $actions = `grep MAX_ACTION $short/db.h`;
    my $rooms = `grep MAX_ROOM $short/db.h`;

    # Add one because the database has the max valid 0-indexed.
    $items =~ /(\d+)/ ;
    $items = $1 + 1;

    $actions =~ /(\d+)/ ;
    $actions = $1 + 1;

    $rooms =~ /(\d+)/ ;
    $rooms = $1 + 1;

    my @moves = `grep "main.0 iteration" $short/logs/trace-log.txt`;
    my $max_moves = pop @moves;
    $max_moves =~ /iteration (\d+)/;
    $max_moves = $1;
    
    my $all_time = `grep user $short/time.txt`;
    $all_time =~ /([\d\.]+)user.*?([\d\.]+)maxres/ ;
    my $cbmc = $1;
    my $mem = $2/1024;
    
    my $solver = `grep Runtime $short/logs/trace-log.txt`;
    $solver =~ /: ([\d\.]+)/ ;
    $solver = $1;
        
    print STATS "$short,$items,$actions,$rooms,$max_moves,$cbmc,$solver,$mem\n";
    
}

close STATS or die $!;

