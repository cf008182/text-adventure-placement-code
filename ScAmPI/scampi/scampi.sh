#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo 'Specify a game and optionally a bound.'
    echo 'Example: ./scampi.sh games/sac/t4.dat 15'
    exit 0
fi


echo Building dumper.
cd dumper && make && cd ..
base=$(basename "$1" .dat)
echo Adventure file: $1

[ -d "$base" ] && echo "Directory $base already exists. Delete before proceeding." && exit 1

echo Creating output directory: $base
cp -r scampi $base
echo Dumping database: $base/db.h
./dumper/dumper "$1" "$base/db.h"
echo Building ScAmPI.
cd "$base" && make && cd ..

if [[ $# -eq 1 ]] ; then
    echo 'Exiting as no bound specified.'
    exit 0
fi

echo Running CBMC.
cd "$base" && (./cbmc.sh $2 | tee logs/trace-log.txt) && cd ..
echo Extracting traces.
cd "$base" && ../extract-traces.pl && cd ..
echo Executing traces.
cd "$base/paths" && ../../log-paths.sh && cd .. && cd ..
echo Commands and logs in: $base/paths/

