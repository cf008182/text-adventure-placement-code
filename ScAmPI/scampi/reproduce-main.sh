#!/bin/bash -x

if ! command -v clang-8 &> /dev/null
then
    echo You need Clang 8. Run:
    echo sudo dpkg --install packages/*.deb
    exit
fi

if [ ! -f llvm-cbe ]; then
    xzcat packages/llvm-cbe.xz > llvm-cbe
    chmod a+x llvm-cbe
fi

date
echo ==========================
echo REPRODUCING SCAMPI RESULTS
echo ==========================
echo 1. Solutions and statistics for t3, t4, t5 and sampler2

./scampi.sh games/sac/t3.dat 9
echo --------------------------
./scampi.sh games/sac/t4.dat 14
echo --------------------------
./scampi.sh games/sac/t5.dat 14
echo --------------------------

./gather-stats.pl

more */paths/*.log

date
echo ==========================
echo REPRODUCING SCAMPI RESULTS
echo ==========================
echo 2. Graph of CBMCs CPU time varying with moves

(cd t4 && ../benchmark-moves.pl)

more t4/move-stats.csv

date
echo ==========================
echo REPRODUCING SCAMPI RESULTS
echo ==========================
echo 3. Graph of CBMCs CPU time varying with actions

./scampi.sh games/sampler2/sampler2.dat
(cd sampler2 && ../benchmark-actions.pl)

more sampler2/action-stats.csv

date
echo ==========================
echo REPRODUCING SCAMPI RESULTS
echo ==========================
echo 4. Reduction in unrolled code size by optimisation for Adventureland

./scampi.sh games/saga/adv01.dat
cd adv01
sloccount engine.c
echo --------------------------

clang-8 -O0 -Wall -c -emit-llvm ScottCurses.c -o bigengine.bc
../llvm-cbe bigengine.bc -o bigengine.c
sloccount bigengine.c
cd ..

date
echo ==========================
echo REPRODUCING SCAMPI RESULTS
echo ==========================
echo 5. Infeasibility of using CBMC without partial evaluation

timeout -v 5m cbmc adv01/nounroll.c -DMOVE_LIMIT=1
echo --------------------------
timeout -v 5m cbmc sampler2/nounroll.c -DMOVE_LIMIT=1
echo --------------------------

cbmc t4/nounroll.c -DMOVE_LIMIT=1
echo --------------------------
cbmc t4/nounroll.c -DMOVE_LIMIT=2
echo --------------------------

cbmc adv01/nounroll.c -DMOVE_LIMIT=1 -DIGNORE_ACTIONS=1 --object-bits 9
echo --------------------------
cbmc sampler2/nounroll.c -DMOVE_LIMIT=1 -DIGNORE_ACTIONS=1
echo --------------------------
cbmc t4/nounroll.c -DIGNORE_ACTIONS=1
echo --------------------------

(cd adv01 && clang-8 -o check.o nounroll.c -DCHECK=1 && ./check.o )

date
echo ==========================
echo REPRODUCING SCAMPI RESULTS
echo ==========================
echo 6. Infeasibility of using CBMC on Adventureland, even with partial evaluation

timeout -v 5m cbmc --all-properties adv01/scampi.c -DMOVE_LIMIT=1 --object-bits 9
echo --------------------------
timeout -v 5m cbmc --all-properties adv01/scampi.c -DMOVE_LIMIT=2 --object-bits 9

date
echo ==========================
echo REPRODUCING SCAMPI RESULTS
echo ==========================
echo 7. ScAmPER cant solve modified Adventureland Sampler

cd scamper
make
./scamper
echo --------------------------

grep SCO paths/*.txt

grep -l SCO paths/*.txt | xargs more
cd ..

date

