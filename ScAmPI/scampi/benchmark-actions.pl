#!/usr/bin/perl
use strict;

my $pwd = `pwd`;
my $game = `basename $pwd`;
(my $max) = ( `grep MAX_ACTION db.h` =~ m/(\d+)/ );
chomp $game;
open (STATS, ">", "action-stats.csv") or die $!;
print STATS "Game Name,Actions,Bitcode size (KB),CPU time (s),Memory (MB),Approx SAT size (MB)\n";
for (my $x = 0; $x <= ($max+1); $x++) {
    my $n = sprintf("%03d", $x);
    `clang-8 -O3 -c -emit-llvm ScottCurses.c -o ScottCurses.bc -DACTION_LIMIT=$x`;
    `/usr/bin/time -o logs/action-time-$n.txt timeout 5m cbmc --dimacs scampi.c -DMOVE_LIMIT=2 > logs/action-log-$n.txt`;
    my $line = `head -1 logs/action-time-$n.txt`;
    if ($line =~ m/Command exited/ ) {
        print STATS "$game,$n,*,*,*,*\n";
        close STATS or die $!;
        exit(0);
    }
    (my $cpu, my $mem) = ($line =~ m/^(.*?)user.*?(\d+)maxres/ );
    $mem = $mem / 1024;
    my $size = (-s "logs/action-log-$n.txt") / (2**20);
    my $bc = (-s "ScottCurses.bc") / (1024);
    print STATS "$game,$n,$bc,$cpu,$mem,$size\n";
}

close STATS or die $!;

