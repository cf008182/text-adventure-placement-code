#!/usr/bin/perl
use strict;

my $pwd = `pwd`;
my $game = `basename $pwd`;
chomp $game;
open (STATS, ">", "move-stats.csv") or die $!;
print STATS "Game Name,Moves,CPU time (s),Memory (MB),Approx SAT size (MB)\n";
for (my $x = 0; $x <= 20; $x++) {
    my $n = sprintf("%02d", $x);
    `/usr/bin/time -o logs/move-time-$n.txt timeout 5m cbmc --dimacs scampi.c -DMOVE_LIMIT=$x > logs/move-log-$n.txt`;
    my $line = `head -1 logs/move-time-$n.txt`;
    if ($line =~ m/Command exited/ ) {
        print STATS "$game,$n,*,*,*\n";
        close STATS or die $!;
        exit(0);
    }
    (my $cpu, my $mem) = ($line =~ m/^(.*?)user.*?(\d+)maxres/ );
    $mem = $mem / 1024;
    my $size = (-s "logs/move-log-$n.txt") / (2**20);
    print STATS "$game,$n,$cpu,$mem,$size\n";
}

close STATS or die $!;

