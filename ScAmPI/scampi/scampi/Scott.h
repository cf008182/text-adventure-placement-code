/*
 *	SCOTT FREE
 *
 *	A free Scott Adams style adventure interpreter
 *
 *	Copyright:
 *	    This software is placed under the GNU license.
 *
 *	Statement:
 *	    Everything in this program has been deduced or obtained solely
 *	from published material. No game interpreter code has been
 *	disassembled, only published BASIC sources (PC-SIG, and Byte Dec
 *	1980) have been used.
 */
 
/*
 *	Controlling block
 */
  
#define LIGHT_SOURCE	9		/* Always 9 how odd */
#define CARRIED		255		/* Carried */
#define DESTROYED	0		/* Destroyed */
#define DARKBIT		15
#define LIGHTOUTBIT	16		/* Light gone out */

#define INF_LIGHT	255

// Const annotations added to encourage compiler optimisation.
 
typedef struct
{
	const unsigned short Vocab;
	const unsigned short Condition[5];
	const unsigned short Action[2];
} Action;

typedef struct
{
	const char *Text;
	const short Exits[6];
} Room;

typedef struct
{
	const char *Text;
	// Location moved into game state, as it's variable.
	const uint8_t InitialLoc;
	const char *AutoGet;
} Item;

// Tail removed, as it's never used.
// Header removed, as all static components are now #defines.
// All dynamic parts of game state moved into GameState struct.

#define YOUARE		1	/* You are not I am */
#define SCOTTLIGHT	2	/* Authentic Scott Adams light messages */
#define DEBUGGING	4	/* Info from database load */
#define TRS80_STYLE	8	/* Display in style used on TRS-80 */
#define PREHISTORIC_LAMP 16	/* Destroy the lamp (very old databases) */

// Useful defines of hard-coded verbs/nouns.
#define VERB_GO		1
#define VERB_GET	10
#define VERB_DROP	18
#define NOUN_NORTH	1
#define NOUN_SOUTH	2
#define NOUN_EAST	3
#define NOUN_WEST	4
#define NOUN_UP		5
#define NOUN_DOWN	6

