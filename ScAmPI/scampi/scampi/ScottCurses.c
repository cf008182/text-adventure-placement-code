/*
 *	ScottFree Revision 1.14
 *
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 *
 *
 *	You must have an ANSI C compiler to build this program.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
// Curses and signals includes removed. - MML
// Size type include added. - MML
#include <stdint.h>

// It's important to declare this extern, or it might get mangled during optimisation,
// preventing CBMC from recognising it.
extern void __CPROVER_assert(int, char*);

#include "Scott.h"

// Configuration for weird platforms removed. - MML

#define INTERPRETER
#include "db.h"

uint32_t BitFlags=0;	/* Might be >32 flags - I haven't seen >32 yet */
uint8_t GameOver = 0;
uint8_t PlayerRoom = PLAYER_START;
uint8_t LightTime = LIGHT_REFILL;
uint8_t RandState = 0;
int8_t CurrentCounter = 0;
uint8_t SavedRoom = 0;
int8_t Counters[16] = {0};
uint8_t RoomSaved[16] = {0};

uint8_t err = 0;

uint8_t Redraw = 0;
uint8_t Options = 0;
// Curses variables removed. - MML

#define MyLoc	(PlayerRoom)


// Prototype for the remnants of main().
void act(int vb, int no);

int RandomPercent(int n)
{
	// Simplify random number generator for consistency. - MML
	RandState = (RandState + 49) % 100;
	if(RandState<n)
		return(1);
	return(0);
}

int CountCarried()
{
	int ct=0;
	int n=0;
	#pragma unroll
	while(ct<=MAX_ITEM)
	{
		if(ItemLocs[ct]==CARRIED)
			n++;
		ct++;
	}
	return(n);
}

MATCHUPITEM

#define CHECKCONDITION(line,cc) \
	{\
		int cv,dv;\
		dv=ACTION_##line##_PARM_##cc ;\
		cv=ACTION_##line##_COND_##cc ;\
		switch(ACTION_##line##_COND_##cc )\
		{\
			case 0:\
				param[pptr++]=dv;\
				break;\
			case 1:\
				if(ItemLocs[dv]!=CARRIED)\
					f2=0;\
				break;\
			case 2:\
				if(ItemLocs[dv]!=MyLoc)\
					f2=0;\
				break;\
			case 3:\
				if(ItemLocs[dv]!=CARRIED&&\
					ItemLocs[dv]!=MyLoc)\
					f2=0;\
				break;\
			case 4:\
				if(MyLoc!=dv)\
					f2=0;\
				break;\
			case 5:\
				if(ItemLocs[dv]==MyLoc)\
					f2=0;\
				break;\
			case 6:\
				if(ItemLocs[dv]==CARRIED)\
					f2=0;\
				break;\
			case 7:\
				if(MyLoc==dv)\
					f2=0;\
				break;\
			case 8:\
				if((BitFlags&(1<<dv))==0)\
					f2=0;\
				break;\
			case 9:\
				if(BitFlags&(1<<dv))\
					f2=0;\
				break;\
			case 10:\
				if(CountCarried()==0)\
					f2=0;\
				break;\
			case 11:\
				if(CountCarried())\
					f2=0;\
				break;\
			case 12:\
				if(ItemLocs[dv]==CARRIED||ItemLocs[dv]==MyLoc)\
					f2=0;\
				break;\
			case 13:\
				if(ItemLocs[dv]==0)\
					f2=0;\
				break;\
			case 14:\
				if(ItemLocs[dv])\
					f2=0;\
				break;\
			case 15:\
				if(CurrentCounter>dv)\
					f2=0;\
				break;\
			case 16:\
				if(CurrentCounter<=dv)\
					f2=0;\
				break;\
			case 17:\
				if(ItemLocs[dv]!=Items[dv].InitialLoc)\
					f2=0;\
				break;\
			case 18:\
				if(ItemLocs[dv]==Items[dv].InitialLoc)\
					f2=0;\
				break;\
			case 19:/* Only seen in Brian Howarth games so far */\
				if(CurrentCounter!=dv)\
					f2=0;\
				break;\
		}\
		if (f2==0) \
			break;\
	}\

#define DOACTION(line,cc) \
	{\
__label__ doneit; \
		if(ACTION_##line##_ACT_##cc >=1 && ACTION_##line##_ACT_##cc <52)\
		{\
		}\
		else if(ACTION_##line##_ACT_##cc >101)\
		{\
		}\
		else switch(ACTION_##line##_ACT_##cc )\
		{\
			case 0:/* NOP */\
				break;\
			case 52:\
				if(CountCarried()==MAX_CARRY)\
				{\
					err = 1;\
					break;\
				}\
				if(ItemLocs[param[pptr]]==MyLoc)\
					Redraw=1;\
				ItemLocs[param[pptr++]]= CARRIED;\
				break;\
			case 53:\
				Redraw=1;\
				ItemLocs[param[pptr++]]=MyLoc;\
				break;\
			case 54:\
				Redraw=1;\
				MyLoc=param[pptr++];\
				break;\
			case 55:\
				if(ItemLocs[param[pptr]]==MyLoc)\
					Redraw=1;\
				ItemLocs[param[pptr++]]=0;\
				break;\
			case 56:\
				BitFlags|=1<<DARKBIT;\
				break;\
			case 57:\
				BitFlags&=~(1<<DARKBIT);\
				break;\
			case 58:\
				BitFlags|=(1<<param[pptr++]);\
				break;\
			case 59:\
				if(ItemLocs[param[pptr]]==MyLoc)\
					Redraw=1;\
				ItemLocs[param[pptr++]]=0;\
				break;\
			case 60:\
				BitFlags&=~(1<<param[pptr++]);\
				break;\
			case 61:\
				BitFlags&=~(1<<DARKBIT);\
				MyLoc=MAX_ROOM;/* It seems to be what the code says! */\
				break;\
			case 62:\
			{\
				/* Bug fix for some systems - before it could get parameters wrong */\
				int i=param[pptr++];\
				ItemLocs[i]=param[pptr++];\
				Redraw=1;\
				break;\
			}\
			case 63:\
doneit: \
				__CPROVER_assert(0, "Game over line " #line );\
				GameOver = 1;\
				return 0;\
			case 64:\
				break;\
			case 65:\
			{\
				int ct=0;\
				int n=0;\
				_Pragma ("unroll") \
				while(ct<=MAX_ITEM)\
				{\
					if(ItemLocs[ct]==TREASURE_ROOM &&\
					  *Items[ct].Text=='*')\
					  	n++;\
					ct++;\
				}\
				if(n==TREASURES)\
				{\
					goto doneit;\
				}\
				break;\
			}\
			case 66:\
			{\
				break;\
			}\
			case 67:\
				BitFlags|=(1<<0);\
				break;\
			case 68:\
				BitFlags&=~(1<<0);\
				break;\
			case 69:\
				LightTime=LIGHT_REFILL;\
				if(ItemLocs[LIGHT_SOURCE]==MyLoc)\
					Redraw=1;\
				ItemLocs[LIGHT_SOURCE]=CARRIED;\
				BitFlags&=~(1<<LIGHTOUTBIT);\
				break;\
			case 70:\
				break;\
			case 71:\
				err = 1;\
				break;\
			case 72:\
			{\
				int i1=param[pptr++];\
				int i2=param[pptr++];\
				int t=ItemLocs[i1];\
				if(t==MyLoc || ItemLocs[i2]==MyLoc)\
					Redraw=1;\
				ItemLocs[i1]=ItemLocs[i2];\
				ItemLocs[i2]=t;\
				break;\
			}\
			case 73:\
				continuation=1;\
				break;\
			case 74:\
				if(ItemLocs[param[pptr]]==MyLoc)\
					Redraw=1;\
				ItemLocs[param[pptr++]]= CARRIED;\
				break;\
			case 75:\
			{\
				int i1,i2;\
				i1=param[pptr++];\
				i2=param[pptr++];\
				if(ItemLocs[i1]==MyLoc)\
					Redraw=1;\
				ItemLocs[i1]=ItemLocs[i2];\
				if(ItemLocs[i2]==MyLoc)\
					Redraw=1;\
				break;\
			}\
			case 76:	/* Looking at adventure .. */\
				break;\
			case 77:\
				if(CurrentCounter>=0)\
					CurrentCounter--;\
				break;\
			case 78:\
				break;\
			case 79:\
				CurrentCounter=param[pptr++];\
				break;\
			case 80:\
			{\
				int t=MyLoc;\
				MyLoc=SavedRoom;\
				SavedRoom=t;\
				Redraw=1;\
				break;\
			}\
			case 81:\
			{\
				/* This is somewhat guessed. Claymorgue always\
				   seems to do select counter n, thing, select counter n,\
				   but uses one value that always seems to exist. Trying\
				   a few options I found this gave sane results on ageing */\
				int t=param[pptr++];\
				int c1=CurrentCounter;\
				CurrentCounter=Counters[t];\
				Counters[t]=c1;\
				break;\
			}\
			case 82:\
				CurrentCounter+=param[pptr++];\
				break;\
			case 83:\
				CurrentCounter-=param[pptr++];\
				if(CurrentCounter< -1)\
					CurrentCounter= -1;\
				/* Note: This seems to be needed. I don't yet \
				   know if there is a maximum value to limit too */\
				break;\
			case 84:\
				break;\
			case 85:\
				break;\
			case 86:\
				break;\
			case 87:\
			{\
				/* Changed this to swap location<->roomflag[x]\
				   not roomflag 0 and x */\
				int p=param[pptr++];\
				int sr=MyLoc;\
				MyLoc=RoomSaved[p];\
				RoomSaved[p]=sr;\
				Redraw=1;\
				break;\
			}\
			case 88:\
				/* Curses code removed. - MML*/ \
				break;\
			case 89:\
				pptr++;\
				/* SAGA draw picture n */\
				/* Spectrum Seas of Blood - start combat ? */\
				/* Poking this into older spectrum games causes a crash */\
				break;\
			default:\
				fprintf(stderr,"Unknown action %d [Param begins %d %d]\n",\
					ACTION_##line##_ACT_##cc ,param[pptr],param[pptr+1]);\
				break;\
		}\
	}\



#define LOOP5(x,n) { { x(n,0) } { x(n,1) } { x(n,2) } { x(n,3) } { x(n,4) } }
#define LOOP4(x,n) { { x(n,0) } { x(n,1) } { x(n,2) } { x(n,3) } }

#ifndef ACTION_LIMIT
	#define CUT_ACTIONS(ct)
#else
	#define CUT_ACTIONS(ct) if (ct >= ACTION_LIMIT) break;
#endif

#define PERFORMLINE(ct) \
do { \
	CUT_ACTIONS(ct) \
	int continuation=0;\
	int param[5],pptr=0;\
	int act[4];\
	int cc=0;\
	f2=-1;\
	LOOP5(CHECKCONDITION,ct) \
	f2=0;\
	/* Actions */ \
	act[0]=Actions[ct].Action[0];\
	act[2]=Actions[ct].Action[1];\
	act[1]=act[0]%150;\
	act[3]=act[2]%150;\
	act[0]/=150;\
	act[2]/=150;\
	cc=0;\
	pptr=0;\
	LOOP4(DOACTION,ct) \
	f2=(1+continuation);\
} while (0);

// Some of the tutorial games don't have a light source, and have sufficiently few items
// that the hard-coded light source is out-of-bounds in the item array.
// Use this macro to check for this, before trying to access the light.
#define GAME_HAS_LIGHT (LIGHT_SOURCE <= MAX_ITEM)

int PerformActions(int vb,int no)
{
	static int disable_sysfunc=0;	/* Recursion lock */
	int d=BitFlags&(1<<DARKBIT);
	
	//int ct=0;
	int fl;
	int doagain=0;
	if(vb==1 && no == -1 )
	{
		//Output("Give me a direction too.");
		err = 1;
		return(0);
	}
	if(vb==1 && no>=1 && no<=6)
	{
		int nl;
		if(GAME_HAS_LIGHT && (ItemLocs[LIGHT_SOURCE]==MyLoc ||
		   ItemLocs[LIGHT_SOURCE]==CARRIED))
		   	d=0;
		//if(d)
		//	Output("Dangerous to move in the dark! ");
		nl=Rooms[MyLoc].Exits[no-1];
		if(nl!=0)
		{
			MyLoc=nl;
			//Look();
			return(0);
		}
		if(d)
		{
			//if(Options&YOUARE)
			//	Output("You fell down and broke your neck. ");
			//else
			//	Output("I fell down and broke my neck. ");
			// Curses code removed. - MML
			GameOver = 1;
			err = 1;
			return 0;
		}
		//if(Options&YOUARE)
		//	Output("You can't go in that direction. ");
		//else
		//	Output("I can't go in that direction. ");
		err = 1;
		return(0);
	}
	fl= -1;

#define CALL_LINE(ct) \
	{\
		int vv,nv;\
		if(ct>0 && ACTION_##ct##_VOCAB !=0)\
			doagain=0;\
		vv=ACTION_##ct##_VOCAB;\
		/* Think this is now right. If a line we run has an action73\
		   run all following lines with vocab of 0,0 */\
		if(vb!=0 && (doagain&&vv!=0))\
			break;\
		/* Oops.. added this minor cockup fix 1.11 */\
		if(vb!=0 && !doagain && fl== 0)\
			break;\
		nv=ACTION_##ct##_NOUN;\
		vv=ACTION_##ct##_VERB;\
		if((vv==vb)||(doagain&&ACTION_##ct##_VOCAB==0))\
		{\
			if((vv==0 && RandomPercent(nv))||doagain||\
				(vv!=0 && (nv==no||nv==0)))\
			{\
				int f2;\
				if(fl== -1)\
					fl= -2;\
				PERFORMLINE(ct)\
				if((f2)>0)\
				{\
					/* Quit immediately if game over.*/\
					if(GameOver)\
						return 0;\
					\
					/* ahah finally figured it out ! */\
					fl=0;\
					if(f2==2)\
						doagain=1;\
					if(vb!=0 && doagain==0)\
						return 0;\
				}\
			}\
		}\
	}

	LOOP_ACT(CALL_LINE)

	if(fl!=0 && disable_sysfunc==0)
	{
		int i;
		if(GAME_HAS_LIGHT && (ItemLocs[LIGHT_SOURCE]==MyLoc ||
		   ItemLocs[LIGHT_SOURCE]==CARRIED))
		   	d=0;
		if(vb==10 || vb==18)
		{
			/* Yes they really _are_ hardcoded values */
			if(vb==10)
			{
				// ALL removed.
				if(no==-1)
				{
					//Output("What ? ");
					err = 1;
					return(0);
				}
				if(CountCarried()==MAX_CARRY)
				{
					err = 1;
					//if(Options&YOUARE)
					//	Output("You are carrying too much. ");
					//else
					//	Output("I've too much to carry. ");
					return(0);
				}
				i=MatchUpItem(no,MyLoc);
				if(i==-1)
				{
					err = 1;
					//if(Options&YOUARE)
					//	Output("It is beyond your power to do that. ");
					//else
					//	Output("It's beyond my power to do that. ");
					return(0);
				}
				ItemLocs[i]= CARRIED;
				//Output("O.K. ");
				Redraw=1;
				return(0);
			}
			if(vb==18)
			{
				// ALL removed.
				if(no==-1)
				{
					//Output("What ? ");
					err = 1;
					return(0);
				}
				i=MatchUpItem(no,CARRIED);
				if(i==-1)
				{
					err = 1;
					//if(Options&YOUARE)
					//	Output("It's beyond your power to do that.\n");
					//else
					//	Output("It's beyond my power to do that.\n");
					return(0);
				}
				ItemLocs[i]=MyLoc;
				//Output("O.K. ");
				Redraw=1;
				return(0);
			}
		}
	}
	return(fl);
}

void act(int vb, int no)
{
	// This is the body of the main loop from main() in the original interpreter.
	// PerformAction(0,0) is moved to the end.
	// It occurs before the player's first move and after every subsequent move.

	switch(PerformActions(vb,no))
	{
		case -1://Output("I don't understand your command. ");
			err = 1;
			break;
		case -2://Output("I can't do that yet. ");
			err = 1;
			break;
	}

	// Don't execute the code for the start of the next move if the game ends here.
	if (GameOver) {
		return;
	}

	/* Brian Howarth games seem to use -1 for forever */
	if(GAME_HAS_LIGHT && (ItemLocs[LIGHT_SOURCE]/*==-1*/!=DESTROYED && LightTime!= INF_LIGHT))
	{
		LightTime--;
		if(LightTime<1)
		{
			BitFlags|=(1<<LIGHTOUTBIT);
			if(ItemLocs[LIGHT_SOURCE]==CARRIED ||
				ItemLocs[LIGHT_SOURCE]==MyLoc)
			{
				//if(Options&SCOTTLIGHT)
				//	Output("Light has run out! ");
				//else
				//	Output("Your light has run out. ");
			}
			if(Options&PREHISTORIC_LAMP)
				ItemLocs[LIGHT_SOURCE]=DESTROYED;
		}
		else if(LightTime<25)
		{
			if(ItemLocs[LIGHT_SOURCE]==CARRIED ||
				ItemLocs[LIGHT_SOURCE]==MyLoc)
			{
		
				//if(Options&SCOTTLIGHT)
				//{
				//	Output("Light runs out in ");
				//	OutputNumber(LightTime);
				//	Output(" turns. ");
				//}
				//else
				//{
				//	if(LightTime%5==0)
				//		Output("Your light is growing dim. ");
				//}
			}
		}
	}
	PerformActions(0,0);
}

