#define WORDS
#include "db.h"

unsigned char nondet_uchar(void);

// Set a sensible default that can be overridden.
#ifndef MOVE_LIMIT
#define MOVE_LIMIT 15
#endif

int main() {
        int n = 0;
        PerformActions(0, 0);
        for (n = 0; n < MOVE_LIMIT; n++) {

                int vb = nondet_uchar();
                int no = nondet_uchar();
                __CPROVER_assume(vb <= MAX_WORD);
                __CPROVER_assume(no <= MAX_WORD);
                __CPROVER_input("verb", Verbs[vb]);
                __CPROVER_input("noun", Nouns[no]);

                act(vb, no);

                // Optional. Removes redundant paths.
                if(err) {
                        return 0;
                }

                if(GameOver) {
                        return 0;
                }

        }

}
