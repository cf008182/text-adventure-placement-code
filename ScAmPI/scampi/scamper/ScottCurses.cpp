/*
 *	ScottFree Revision 1.14
 *
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 *
 *
 *	You must have an ANSI C compiler to build this program.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
// Curses and signals includes removed. - MML
// Size type include added. - MML
#include <stdint.h>
// Assertions added. - MML
#include <assert.h>
// C++ includes. - MML
#include <vector>

#include "Scott.h"

// Configuration for weird platforms removed. - MML

#define INTERPRETER
#include "db.h"

// GameTail removed, as it's never used. - MML
char NounText[16];

// The following moved to GameState: - MML
// int Counters[16];	/* Range unknown */
// int CurrentCounter;
// int SavedRoom;
// int RoomSaved[16];	/* Range unknown */

int Redraw;		/* Update item window */
int Options;		/* Option flags set */
int Width;		/* Terminal width */
// Curses variables removed. - MML

#define TRS80_LINE	"\n<------------------------------------------------------------>\n"

#define MyLoc	(Q.PlayerRoom)

// Moved to GameState. - MML
// long BitFlags=0;	/* Might be >32 flags - I haven't seen >32 yet */

GameState Q;

// Whether an error occurred while executing the command.
int err = 0;
// Which messages were output during execution?
std::vector<uint8_t> messages_seen;
// Which actions were executed?
std::vector<uint8_t> actions_seen;
// Which conditions were true/false?
std::vector<uint8_t> conditions_seen;

// Prototype for the remnants of main().
void act(int vb, int no);

void Fatal(char *x)
{
	// Curses code removed. - MML
	fprintf(stderr,"%s.\n",x);
	exit(1);
}

void *MemAlloc(int size)
{
	void *t=(void *)malloc(size);
	if(t==NULL)
		Fatal("Out of memory");
	return(t);
}

int RandomPercent(int n)
{
	// Simplify random number generator for consistency. - MML
	Q.RandState = (Q.RandState + 49) % 100;
	if(Q.RandState<n)
		return(1);
	return(0);
}

int CountCarried()
{
	int ct=0;
	int n=0;
	while(ct<=MAX_ITEM)
	{
		if(Q.ItemLocs[ct]==CARRIED)
			n++;
		ct++;
	}
	return(n);
}

char *MapSynonym(char *word)
{
	int n=1;
	char *tp;
	static char lastword[16];	/* Last non synonym */
	while(n<=MAX_WORD)
	{
		tp=Nouns[n];
		if(*tp=='*')
			tp++;
		else
			strcpy(lastword,tp);
		if(strncasecmp(word,tp,WORD_LENGTH)==0)
			return(lastword);
		n++;
	}
	return(NULL);
}

int MatchUpItem(char *text, int loc)
{
	char *word=MapSynonym(text);
	int ct=0;
	
	if(word==NULL)
		word=text;
	
	while(ct<=MAX_ITEM)
	{
		if(Items[ct].AutoGet && Q.ItemLocs[ct]==loc &&
			strncasecmp(Items[ct].AutoGet,word,WORD_LENGTH)==0)
			return(ct);
		ct++;
	}
	return(-1);
}

// ReadString and LoadDatabase removed, as no longer needed. - MML

// All output code removed, as no longer needed. - MML
		
int WhichWord(char *word, char **list)
{
	int n=1;
	int ne=1;
	char *tp;
	while(ne<=MAX_WORD)
	{
		tp=list[ne];
		if(*tp=='*')
			tp++;
		else
			n=ne;
		if(strncasecmp(word,tp,WORD_LENGTH)==0)
			return(n);
		ne++;
	}
	return(-1);
}

// Function to test all conditions in action lines for MC/DC coverage. - MML

int CountConditions() {
	int ct = 0;
	int conditions = 0;
	while(ct<=MAX_ACTION) {
		int cc = 0;
		int valid = 0;
		while(cc<5)
		{
			int cv;
			//int dv;
			cv=Actions[ct].Condition[cc];
			cv%=20;
			switch(cv)
			{
				case 0:
					break;
				default:
					valid++;
					break;
			}
			cc++;
		}
		// Including actions with no conditions makes the percentages work.
		//if (valid > 0) {
			conditions = conditions + (1 << valid);		
		//}
		ct++;
	}
	return conditions;
}

void TestAllConditions() {
	int ct = 0;
	// Iterate over all actions.
	while(ct<=MAX_ACTION)
	{
		int seen=0;
		int cc=0;
		// Iterate over all conditions.
		// Don't stop even if one is false, unlike normal evaluation.
		while(cc<5)
		{
			int cv,dv;
			int result = 0;
			cv=Actions[ct].Condition[cc];
			dv=cv/20;
			cv%=20;
			// Is the condition true/false? Store it in result.
			switch(cv)
			{
				case 0:
					break;
				case 1:
					result = !(Q.ItemLocs[dv]!=CARRIED);
					break;
				case 2:
					result = !(Q.ItemLocs[dv]!=MyLoc);
					break;
				case 3:
					result = !(Q.ItemLocs[dv]!=CARRIED&&
						Q.ItemLocs[dv]!=MyLoc);
					break;
				case 4:
					result = !(MyLoc!=dv);
					break;
				case 5:
					result = !(Q.ItemLocs[dv]==MyLoc);
					break;
				case 6:
					result = !(Q.ItemLocs[dv]==CARRIED);
					break;
				case 7:
					result = !(MyLoc==dv);
					break;
				case 8:
					result = !((Q.BitFlags&(1<<dv))==0);
					break;
				case 9:
					result = !(Q.BitFlags&(1<<dv));
					break;
				case 10:
					result = !(CountCarried()==0);
					break;
				case 11:
					result = !(CountCarried());
					break;
				case 12:
					result = !(Q.ItemLocs[dv]==CARRIED||Q.ItemLocs[dv]==MyLoc);
					break;
				case 13:
					result = !(Q.ItemLocs[dv]==0);
					break;
				case 14:
					result = !(Q.ItemLocs[dv]);
					break;
				case 15:
					result = !(Q.CurrentCounter>dv);
					break;
				case 16:
					result = !(Q.CurrentCounter<=dv);
					break;
				case 17:
					result = !(Q.ItemLocs[dv]!=Items[dv].InitialLoc);
					break;
				case 18:
					result = !(Q.ItemLocs[dv]==Items[dv].InitialLoc);
					break;
				case 19:/* Only seen in Brian Howarth games so far */
					result = !(Q.CurrentCounter!=dv);
					break;
			}
			// Save the condition by setting a bit.
			seen = seen | (result << cc);
			cc++;
		}

		// Add the string of bits to the vector.
		conditions_seen.push_back(seen);
		ct++;
	}
}


// LineInput function removed. Uses replaced with fgets(). - MML
// GetInput removed. Verb/noun number now passed directly. - MML
// SaveGame and LoadGame removed. No longer relevant in non-interactive setting. - MML

int PerformLine(int ct)
{
	int continuation=0;
	int param[5],pptr=0;
	int act[4];
	int cc=0;
	while(cc<5)
	{
		int cv,dv;
		cv=Actions[ct].Condition[cc];
		dv=cv/20;
		cv%=20;
		switch(cv)
		{
			case 0:
				param[pptr++]=dv;
				break;
			case 1:
				if(Q.ItemLocs[dv]!=CARRIED)
					return(0);
				break;
			case 2:
				if(Q.ItemLocs[dv]!=MyLoc)
					return(0);
				break;
			case 3:
				if(Q.ItemLocs[dv]!=CARRIED&&
					Q.ItemLocs[dv]!=MyLoc)
					return(0);
				break;
			case 4:
				if(MyLoc!=dv)
					return(0);
				break;
			case 5:
				if(Q.ItemLocs[dv]==MyLoc)
					return(0);
				break;
			case 6:
				if(Q.ItemLocs[dv]==CARRIED)
					return(0);
				break;
			case 7:
				if(MyLoc==dv)
					return(0);
				break;
			case 8:
				if((Q.BitFlags&(1<<dv))==0)
					return(0);
				break;
			case 9:
				if(Q.BitFlags&(1<<dv))
					return(0);
				break;
			case 10:
				if(CountCarried()==0)
					return(0);
				break;
			case 11:
				if(CountCarried())
					return(0);
				break;
			case 12:
				if(Q.ItemLocs[dv]==CARRIED||Q.ItemLocs[dv]==MyLoc)
					return(0);
				break;
			case 13:
				if(Q.ItemLocs[dv]==0)
					return(0);
				break;
			case 14:
				if(Q.ItemLocs[dv])
					return(0);
				break;
			case 15:
				if(Q.CurrentCounter>dv)
					return(0);
				break;
			case 16:
				if(Q.CurrentCounter<=dv)
					return(0);
				break;
			case 17:
				if(Q.ItemLocs[dv]!=Items[dv].InitialLoc)
					return(0);
				break;
			case 18:
				if(Q.ItemLocs[dv]==Items[dv].InitialLoc)
					return(0);
				break;
			case 19:/* Only seen in Brian Howarth games so far */
				if(Q.CurrentCounter!=dv)
					return(0);
				break;
		}
		cc++;
	}
	/* Actions */
	// Record that the action was executed.
	actions_seen.push_back(ct);
	act[0]=Actions[ct].Action[0];
	act[2]=Actions[ct].Action[1];
	act[1]=act[0]%150;
	act[3]=act[2]%150;
	act[0]/=150;
	act[2]/=150;
	cc=0;
	pptr=0;
	while(cc<4)
	{
		if(act[cc]>=1 && act[cc]<52)
		{
			//Output(Messages[act[cc]]);
			//Output("\n");
			// Record that the message was output.
			messages_seen.push_back(act[cc]);
		}
		else if(act[cc]>101)
		{
			//Output(Messages[act[cc]-50]);
			//Output("\n");
			// Record that the messag was output.
			messages_seen.push_back(act[cc]-50);
		}
		else switch(act[cc])
		{
			case 0:/* NOP */
				break;
			case 52:
				if(CountCarried()==MAX_CARRY)
				{
					err = 1;
					//if(Options&YOUARE)
					//	Output("You are carrying too much. ");
					//else
					//	Output("I've too much to carry! ");
					break;
				}
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]= CARRIED;
				break;
			case 53:
				Redraw=1;
				Q.ItemLocs[param[pptr++]]=MyLoc;
				break;
			case 54:
				Redraw=1;
				MyLoc=param[pptr++];
				break;
			case 55:
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]=0;
				break;
			case 56:
				Q.BitFlags|=1<<DARKBIT;
				break;
			case 57:
				Q.BitFlags&=~(1<<DARKBIT);
				break;
			case 58:
				Q.BitFlags|=(1<<param[pptr++]);
				break;
			case 59:
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]=0;
				break;
			case 60:
				Q.BitFlags&=~(1<<param[pptr++]);
				break;
			case 61:
				//if(Options&YOUARE)
				//	Output("You are dead.\n");
				//else
				//	Output("I am dead.\n");
				Q.BitFlags&=~(1<<DARKBIT);
				MyLoc=MAX_ROOM;/* It seems to be what the code says! */
				//Look();
				break;
			case 62:
			{
				/* Bug fix for some systems - before it could get parameters wrong */
				int i=param[pptr++];
				Q.ItemLocs[i]=param[pptr++];
				Redraw=1;
				break;
			}
			case 63:
doneit:				//Output("The game is now over.\n");
				// Curses code removed. - MML
				Q.GameOver = 1;
				// Return value ensures correct branch in calling code to break out. - MML
				return 1;
			case 64:
				//Look();
				break;
			case 65:
			{
				int ct=0;
				int n=0;
				while(ct<=MAX_ITEM)
				{
					if(Q.ItemLocs[ct]==TREASURE_ROOM &&
					  *Items[ct].Text=='*')
					  	n++;
					ct++;
				}
				//if(Options&YOUARE)
				//	Output("You have stored ");
				//else
				//	Output("I've stored ");
				//OutputNumber(n);
				//Output(" treasures.  On a scale of 0 to 100, that rates ");
				//OutputNumber((n*100)/TREASURES);
				//Output(".\n");
				if(n==TREASURES)
				{
					//Output("Well done.\n");
					messages_seen.push_back(MAX_MESSAGE);
					goto doneit;
				}
				break;
			}
			case 66:
			{
				//int ct=0;
				//int f=0;
				//if(Options&YOUARE)
				//	Output("You are carrying:\n");
				//else
				//	Output("I'm carrying:\n");
				//while(ct<=MAX_ITEM)
				//{
				//	if(Q.ItemLocs[ct]==CARRIED)
				//	{
				//		if(f==1)
				//		{
				//			if (Options & TRS80_STYLE)
				//				Output(". ");
				//			else
				//				Output(" - ");
				//		}
				//		f=1;
				//		Output(Items[ct].Text);
				//	}
				//	ct++;
				//}
				//if(f==0)
				//	Output("Nothing");
				//Output(".\n");
				break;
			}
			case 67:
				Q.BitFlags|=(1<<0);
				break;
			case 68:
				Q.BitFlags&=~(1<<0);
				break;
			case 69:
				Q.LightTime=LIGHT_REFILL;
				if(Q.ItemLocs[LIGHT_SOURCE]==MyLoc)
					Redraw=1;
				Q.ItemLocs[LIGHT_SOURCE]=CARRIED;
				Q.BitFlags&=~(1<<LIGHTOUTBIT);
				break;
			case 70:
				//ClearScreen(); /* pdd. */
				//OutReset();
				break;
			case 71:
				//SaveGame();
				err = 1;
				break;
			case 72:
			{
				int i1=param[pptr++];
				int i2=param[pptr++];
				int t=Q.ItemLocs[i1];
				if(t==MyLoc || Q.ItemLocs[i2]==MyLoc)
					Redraw=1;
				Q.ItemLocs[i1]=Q.ItemLocs[i2];
				Q.ItemLocs[i2]=t;
				break;
			}
			case 73:
				continuation=1;
				break;
			case 74:
				if(Q.ItemLocs[param[pptr]]==MyLoc)
					Redraw=1;
				Q.ItemLocs[param[pptr++]]= CARRIED;
				break;
			case 75:
			{
				int i1,i2;
				i1=param[pptr++];
				i2=param[pptr++];
				if(Q.ItemLocs[i1]==MyLoc)
					Redraw=1;
				Q.ItemLocs[i1]=Q.ItemLocs[i2];
				if(Q.ItemLocs[i2]==MyLoc)
					Redraw=1;
				break;
			}
			case 76:	/* Looking at adventure .. */
				//Look();
				break;
			case 77:
				if(Q.CurrentCounter>=0)
					Q.CurrentCounter--;
				break;
			case 78:
				//OutputNumber(Q.CurrentCounter);
				break;
			case 79:
				Q.CurrentCounter=param[pptr++];
				break;
			case 80:
			{
				int t=MyLoc;
				MyLoc=Q.SavedRoom;
				Q.SavedRoom=t;
				Redraw=1;
				break;
			}
			case 81:
			{
				/* This is somewhat guessed. Claymorgue always
				   seems to do select counter n, thing, select counter n,
				   but uses one value that always seems to exist. Trying
				   a few options I found this gave sane results on ageing */
				int t=param[pptr++];
				int c1=Q.CurrentCounter;
				Q.CurrentCounter=Q.Counters[t];
				Q.Counters[t]=c1;
				break;
			}
			case 82:
				Q.CurrentCounter+=param[pptr++];
				break;
			case 83:
				Q.CurrentCounter-=param[pptr++];
				if(Q.CurrentCounter< -1)
					Q.CurrentCounter= -1;
				/* Note: This seems to be needed. I don't yet
				   know if there is a maximum value to limit too */
				break;
			case 84:
				//Output(NounText);
				break;
			case 85:
				//Output(NounText);
				//Output("\n");
				break;
			case 86:
				//Output("\n");
				break;
			case 87:
			{
				/* Changed this to swap location<->roomflag[x]
				   not roomflag 0 and x */
				int p=param[pptr++];
				int sr=MyLoc;
				MyLoc=Q.RoomSaved[p];
				Q.RoomSaved[p]=sr;
				Redraw=1;
				break;
			}
			case 88:
				// Curses code removed. - MML
				break;
			case 89:
				pptr++;
				/* SAGA draw picture n */
				/* Spectrum Seas of Blood - start combat ? */
				/* Poking this into older spectrum games causes a crash */
				break;
			default:
				fprintf(stderr,"Unknown action %d [Param begins %d %d]\n",
					act[cc],param[pptr],param[pptr+1]);
				break;
		}
		cc++;
	}
	return(1+continuation);		
}


int PerformActions(int vb,int no)
{
	static int disable_sysfunc=0;	/* Recursion lock */
	int d=Q.BitFlags&(1<<DARKBIT);
	
	int ct=0;
	int fl;
	int doagain=0;
	if(vb==1 && no == -1 )
	{
		//Output("Give me a direction too.");
		err = 1;
		return(0);
	}
	if(vb==1 && no>=1 && no<=6)
	{
		int nl;
		if(Q.ItemLocs[LIGHT_SOURCE]==MyLoc ||
		   Q.ItemLocs[LIGHT_SOURCE]==CARRIED)
		   	d=0;
		//if(d)
		//	Output("Dangerous to move in the dark! ");
		nl=Rooms[MyLoc].Exits[no-1];
		if(nl!=0)
		{
			MyLoc=nl;
			//Look();
			return(0);
		}
		if(d)
		{
			//if(Options&YOUARE)
			//	Output("You fell down and broke your neck. ");
			//else
			//	Output("I fell down and broke my neck. ");
			// Curses code removed. - MML
			Q.GameOver = 1;
			err = 1;
			return 0;
		}
		//if(Options&YOUARE)
		//	Output("You can't go in that direction. ");
		//else
		//	Output("I can't go in that direction. ");
		err = 1;
		return(0);
	}
	fl= -1;
	while(ct<=MAX_ACTION)
	{
		int vv,nv;
		vv=Actions[ct].Vocab;
		/* Think this is now right. If a line we run has an action73
		   run all following lines with vocab of 0,0 */
		if(vb!=0 && (doagain&&vv!=0))
			break;
		/* Oops.. added this minor cockup fix 1.11 */
		if(vb!=0 && !doagain && fl== 0)
			break;
		nv=vv%150;
		vv/=150;
		if((vv==vb)||(doagain&&Actions[ct].Vocab==0))
		{
			if((vv==0 && RandomPercent(nv))||doagain||
				(vv!=0 && (nv==no||nv==0)))
			{
				int f2;
				if(fl== -1)
					fl= -2;
				if((f2=PerformLine(ct))>0)
				{
					// Quit immediately if game over.
					if(Q.GameOver)
						return 0;
					
					/* ahah finally figured it out ! */
					fl=0;
					if(f2==2)
						doagain=1;
					if(vb!=0 && doagain==0)
						return 0;
				}
			}
		}
		ct++;
		if(ct<=MAX_ACTION && Actions[ct].Vocab!=0)
			doagain=0;
	}
	if(fl!=0 && disable_sysfunc==0)
	{
		int i;
		if(Q.ItemLocs[LIGHT_SOURCE]==MyLoc ||
		   Q.ItemLocs[LIGHT_SOURCE]==CARRIED)
		   	d=0;
		if(vb==10 || vb==18)
		{
			/* Yes they really _are_ hardcoded values */
			if(vb==10)
			{
				if(strcasecmp(NounText,"ALL")==0)
				{
					int ct=0;
					int f=0;
					
					if(d)
					{
						//Output("It is dark.\n");
						return 0;
					}
					while(ct<=MAX_ITEM)
					{
						if(Q.ItemLocs[ct]==MyLoc && Items[ct].AutoGet!=NULL && Items[ct].AutoGet[0]!='*')
						{
							no=WhichWord(Items[ct].AutoGet,Nouns);
							disable_sysfunc=1;	/* Don't recurse into auto get ! */
							PerformActions(vb,no);	/* Recursively check each items table code */
							disable_sysfunc=0;
							if(CountCarried()==MAX_CARRY)
							{
								//if(Options&YOUARE)
								//	Output("You are carrying too much. ");
								//else
								//	Output("I've too much to carry. ");
								return(0);
							}
						 	Q.ItemLocs[ct]= CARRIED;
						 	Redraw=1;
						 	//OutBuf(Items[ct].Text);
						 	//Output(": O.K.\n");
						 	f=1;
						 }
						 ct++;
					}
					if(f==0) {
						//Output("Nothing taken.");
						err = 1;
					}
					return(0);
				}
				if(no==-1)
				{
					//Output("What ? ");
					err = 1;
					return(0);
				}
				if(CountCarried()==MAX_CARRY)
				{
					err = 1;
					//if(Options&YOUARE)
					//	Output("You are carrying too much. ");
					//else
					//	Output("I've too much to carry. ");
					return(0);
				}
				i=MatchUpItem(NounText,MyLoc);
				if(i==-1)
				{
					err = 1;
					//if(Options&YOUARE)
					//	Output("It is beyond your power to do that. ");
					//else
					//	Output("It's beyond my power to do that. ");
					return(0);
				}
				Q.ItemLocs[i]= CARRIED;
				//Output("O.K. ");
				Redraw=1;
				return(0);
			}
			if(vb==18)
			{
				if(strcasecmp(NounText,"ALL")==0)
				{
					int ct=0;
					int f=0;
					while(ct<=MAX_ITEM)
					{
						if(Q.ItemLocs[ct]==CARRIED && Items[ct].AutoGet && Items[ct].AutoGet[0]!='*')
						{
							no=WhichWord(Items[ct].AutoGet,Nouns);
							disable_sysfunc=1;
							PerformActions(vb,no);
							disable_sysfunc=0;
							Q.ItemLocs[ct]=MyLoc;
							//OutBuf(Items[ct].Text);
							//Output(": O.K.\n");
							Redraw=1;
							f=1;
						}
						ct++;
					}
					if(f==0) {
						err = 1;
						//Output("Nothing dropped.\n");
					}
					return(0);
				}
				if(no==-1)
				{
					//Output("What ? ");
					err = 1;
					return(0);
				}
				i=MatchUpItem(NounText,CARRIED);
				if(i==-1)
				{
					err = 1;
					//if(Options&YOUARE)
					//	Output("It's beyond your power to do that.\n");
					//else
					//	Output("It's beyond my power to do that.\n");
					return(0);
				}
				Q.ItemLocs[i]=MyLoc;
				//Output("O.K. ");
				Redraw=1;
				return(0);
			}
		}
	}
	return(fl);
}

GameState InitGame() {
	Q = InitState;
	err = 0;
	messages_seen.clear();
	actions_seen.clear();
	conditions_seen.clear();
	PerformActions(0, 0);
	TestAllConditions();
	return Q;
}

GameState NextState(GameState q0, int vb, int no) {
	//printf("Entering next state, %d, %d.\n", vb, no);
	assert(!q0.GameOver);
	Q = q0;
	err = 0;
	messages_seen.clear();
	actions_seen.clear();
	conditions_seen.clear();
	act(vb, no);
	//printf("Leaving next state.\n");
	TestAllConditions();
	return Q;
}

int GetError() {
	return err;
}
	
void act(int vb, int no)
{
	// This is the body of the main loop from main() in the original interpreter.
	// PerformAction(0,0) is moved to the end.
	// It occurs before the player's first move and after every subsequent move.

	//GetInput(&vb,&no);
	// Copy the text of the noun into NounText.
	// This is needed for take/drop to work.
	strcpy(NounText, Nouns[no]);

	switch(PerformActions(vb,no))
	{
		case -1://Output("I don't understand your command. ");
			err = 1;
			break;
		case -2://Output("I can't do that yet. ");
			err = 1;
			break;
	}

	// Don't execute the code for the start of the next move if the game ends here.
	if (Q.GameOver) {
		return;
	}

	/* Brian Howarth games seem to use -1 for forever */
	if(Q.ItemLocs[LIGHT_SOURCE]/*==-1*/!=DESTROYED && Q.LightTime!= INF_LIGHT)
	{
		Q.LightTime--;
		if(Q.LightTime<1)
		{
			Q.BitFlags|=(1<<LIGHTOUTBIT);
			if(Q.ItemLocs[LIGHT_SOURCE]==CARRIED ||
				Q.ItemLocs[LIGHT_SOURCE]==MyLoc)
			{
				//if(Options&SCOTTLIGHT)
				//	Output("Light has run out! ");
				//else
				//	Output("Your light has run out. ");
			}
			if(Options&PREHISTORIC_LAMP)
				Q.ItemLocs[LIGHT_SOURCE]=DESTROYED;
		}
		else if(Q.LightTime<25)
		{
			if(Q.ItemLocs[LIGHT_SOURCE]==CARRIED ||
				Q.ItemLocs[LIGHT_SOURCE]==MyLoc)
			{
		
				//if(Options&SCOTTLIGHT)
				//{
				//	Output("Light runs out in ");
				//	OutputNumber(Q.LightTime);
				//	Output(" turns. ");
				//}
				//else
				//{
				//	if(Q.LightTime%5==0)
				//		Output("Your light is growing dim. ");
				//}
			}
		}
	}
	PerformActions(0,0);
}
