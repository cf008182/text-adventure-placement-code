#define MAX_ITEM 31
#define MAX_ACTION 99
#define MAX_WORD 66
#define MAX_ROOM 17
#define MAX_CARRY 6
#define PLAYER_START 8
#define TREASURES 3
#define WORD_LENGTH 3
#define MAX_MESSAGE 59
#define TREASURE_ROOM 3
#define LIGHT_REFILL 125

typedef struct {
	uint32_t BitFlags;
	uint8_t GameOver;
	uint8_t PlayerRoom;
	uint8_t LightTime;
	uint8_t RandState;
	int8_t CurrentCounter;
	uint8_t SavedRoom;
	int8_t Counters[16];
	uint8_t RoomSaved[16];
	uint8_t ItemLocs[MAX_ITEM + 1];
} GameState;
#ifdef INTERPRETER
Item Items[] = {
    {
        "Dark hole", // Text
        4, // InitialLoc
        NULL, // AutoGet
    },
    {
        "*Pot of RUBIES*", // Text
        4, // InitialLoc
        "RUB", // AutoGet
    },
    {
        "Spider web with writing on it", // Text
        2, // InitialLoc
        NULL, // AutoGet
    },
    {
        "-HOLLOW- stump and remains of a felled tree", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Cypress tree", // Text
        1, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Water", // Text
        7, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Evil smelling mud", // Text
        1, // InitialLoc
        "MUD", // AutoGet
    },
    {
        "Fish", // Text
        7, // InitialLoc
        "FIS", // AutoGet
    },
    {
        "Rusty axe (Magic word 'BUNYON' on it)", // Text
        7, // InitialLoc
        "AXE", // AutoGet
    },
    {
        "Water in bottle", // Text
        3, // InitialLoc
        "BOT", // AutoGet
    },
    {
        "Empty bottle", // Text
        0, // InitialLoc
        "BOT", // AutoGet
    },
    {
        "Ring of skeleton keys", // Text
        2, // InitialLoc
        "KEY", // AutoGet
    },
    {
        "Sign 'Leave *TREASURES* here, then say: SCORE'", // Text
        3, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Locked door", // Text
        5, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Open door with a hallway beyond", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Swamp gas", // Text
        1, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Chigger bites", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Infected chigger bites", // Text
        0, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Patches of 'OILY' slime", // Text
        1, // InitialLoc
        "OIL", // AutoGet
    },
    {
        "Large sleeping dragon", // Text
        9, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Flint & steel", // Text
        15, // InitialLoc
        "FLI", // AutoGet
    },
    {
        "Sign here says 'In many cases mud is good. In others...'", // Text
        9, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Chiggers", // Text
        1, // InitialLoc
        "CHI", // AutoGet
    },
    {
        "*JEWELED FRUIT*", // Text
        11, // InitialLoc
        "FRU", // AutoGet
    },
    {
        "*Small statue of a BLUE OX*", // Text
        12, // InitialLoc
        "OX", // AutoGet
    },
    {
        "Sign says 'LIMBO. Find right exit and live again!'", // Text
        17, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Sign says 'No swimming allowed here'", // Text
        7, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Dead fish", // Text
        0, // InitialLoc
        "FIS", // AutoGet
    },
    {
        "Sign says 'Paul's place'", // Text
        11, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Trees", // Text
        8, // InitialLoc
        NULL, // AutoGet
    },
    {
        "Large outdoor Advertisement", // Text
        14, // InitialLoc
        "ADV", // AutoGet
    },
    {
        "Hole", // Text
        14, // InitialLoc
        NULL, // AutoGet
    },
};
char* Verbs[] = {
    "<auto>",
    "GO",
    "*ENT",
    "*RUN",
    "*WAL",
    "*CLI",
    "TOS",
    "*THR",
    "LOC",
    "*FIN",
    "TAK",
    "*GET",
    "*PIC",
    "*CAT",
    "YEL",
    "*SCR",
    "*HOL",
    "AT",
    "DROP",
    "KIS",
    "*WAV",
    "*TIC",
    "*KIC",
    "*TOU",
    "*FEE",
    "*FUC",
    "*HIT",
    "*POK",
    "GIV",
    "*DRO",
    "*REL",
    "*SPI",
    "*LEA",
    "*POU",
    "LOO",
    "*EXA",
    "*DES",
    "SPE",
    "*SAY",
    "*CAL",
    "SCO",
    "SWI",
    "BUI",
    "*MAK",
    "DRI",
    "*EAT",
    "INV",
    "IGN",
    "*LIG",
    "*.",
    "*BUR",
    "SLA",
    "*ATT",
    "*KIL",
    "OPE",
    "WAK",
    "JUM",
    "HEL",
    "RUB",
    "CRO",
    "CHO",
    "*CUT",
    "STO",
    "REA",
    "UNL",
    "QUI",
    "SAV",
};
char* Nouns[] = {
    "<any>",
    "NORTH",
    "SOUTH",
    "EAST",
    "WEST",
    "UP",
    "DOWN",
    "SIG",
    "LAV",
    "*DAM",
    "CHI",
    "*BIT",
    "TRE",
    "*STU",
    "DOO",
    "BUN",
    "GAS",
    "OX",
    "5",
    "FRU",
    "KEY",
    "ADV",
    "INV",
    "BOT",
    "*CON",
    "HOL",
    "SHO",
    "*BAN",
    "50",
    "10",
    "OIL",
    "*SLI",
    "SWA",
    "DRA",
    "HAL",
    "FLI",
    "30",
    "WEB",
    "*WRI",
    "AXE",
    "*AX",
    "MUD",
    "*MED",
    "FIS",
    "RUB",
    "GAM",
    "WAT",
    "SPI",
    "WIN",
    "8",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
};
Room Rooms[] = {
    {
        "[nowhere]", // Text
        {0, 0, 0, 0, 0, 0, }
    },
    {
        "dismal swamp", // Text
        {9, 0, 14, 11, 0, 0, }
    },
    {
        "top of a tall cypress tree", // Text
        {0, 0, 0, 0, 0, 1, }
    },
    {
        "damp hollow stump in the swamp", // Text
        {0, 0, 0, 0, 1, 4, }
    },
    {
        "root chamber under the stump", // Text
        {0, 0, 0, 0, 3, 0, }
    },
    {
        "semi-dark hole by the root chamber", // Text
        {0, 0, 0, 0, 4, 0, }
    },
    {
        "long down sloping hall", // Text
        {0, 0, 0, 0, 5, 0, }
    },
    {
        "*I'm on the shore of a lake", // Text
        {12, 14, 0, 9, 0, 0, }
    },
    {
        "forest", // Text
        {8, 8, 9, 8, 0, 0, }
    },
    {
        "sunny meadow", // Text
        {0, 1, 7, 8, 0, 0, }
    },
    {
        "*I think I'm in real trouble now. There's a fellow here with\na pitchfork and pointed tail. ...Oh Hell!", // Text
        {0, 0, 0, 0, 0, 0, }
    },
    {
        "hidden grove", // Text
        {8, 0, 1, 0, 0, 0, }
    },
    {
        "quick-sand bog", // Text
        {0, 0, 0, 0, 0, 0, }
    },
    {
        "top of an oak.\nTo the East I see a meadow, beyond that a lake.", // Text
        {0, 0, 0, 0, 0, 8, }
    },
    {
        "*I'm at the edge of a BOTTOMLESS hole", // Text
        {7, 0, 0, 1, 0, 0, }
    },
    {
        "*I'm on a ledge just below the rim of the BOTTOMLESS hole. I\ndon't think I want to go down", // Text
        {0, 0, 0, 0, 14, 10, }
    },
    {
        "*I'm in an endless corridor", // Text
        {16, 17, 16, 16, 16, 16, }
    },
    {
        "large misty room with strange\nunreadable letters over all the exits.", // Text
        {16, 10, 8, 10, 13, 10, }
    },
};
Action Actions[] = {
    {10, {341, 347, 0, 0, 0}, {211, 0}},
    {10, {321, 126, 340, 320, 0}, {12902, 11159}},
    {100, {204, 0, 0, 0, 0}, {513, 0}},
    {5, {121, 186, 120, 20, 0}, {662, 0}},
    {8, {326, 346, 442, 126, 320}, {11186, 750}},
    {100, {104, 308, 0, 0, 0}, {8626, 0}},
    {50, {141, 186, 140, 540, 0}, {959, 7800}},
    {100, {148, 140, 480, 220, 0}, {9062, 9900}},
    {30, {441, 346, 326, 126, 320}, {11186, 750}},
    {50, {382, 123, 0, 0, 0}, {10507, 9150}},
    {100, {288, 200, 280, 0, 0}, {11160, 9150}},
    {100, {248, 240, 0, 0, 0}, {9660, 0}},
    {100, {269, 260, 0, 0, 0}, {1258, 1423}},
    {0, {0, 0, 0, 0, 0}, {1511, 1813}},
    {5121, {602, 0, 0, 0, 0}, {2100, 0}},
    {5112, {62, 0, 0, 0, 0}, {2250, 0}},
    {1541, {122, 341, 340, 120, 0}, {8902, 2417}},
    {1541, {122, 321, 320, 120, 0}, {8902, 2417}},
    {1522, {0, 0, 0, 0, 0}, {9900, 0}},
    {6900, {0, 0, 0, 0, 0}, {9900, 0}},
    {7050, {406, 0, 0, 0, 0}, {2700, 0}},
    {7066, {302, 401, 0, 0, 0}, {2850, 0}},
    {1232, {0, 0, 0, 0, 0}, {3000, 0}},
    {6619, {461, 460, 0, 0, 0}, {2609, 0}},
    {1546, {102, 201, 200, 180, 0}, {8902, 2400}},
    {1546, {102, 206, 0, 0, 0}, {3150, 0}},
    {9487, {42, 0, 0, 0, 0}, {3300, 0}},
    {162, {82, 40, 0, 0, 0}, {8170, 9600}},
    {9012, {82, 232, 161, 80, 60}, {8303, 3450}},
    {8114, {104, 262, 226, 0, 0}, {3600, 0}},
    {9614, {104, 262, 226, 0, 0}, {3600, 0}},
    {939, {161, 60, 160, 0, 0}, {3808, 7950}},
    {8114, {262, 221, 260, 280, 0}, {8303, 11400}},
    {8850, {0, 0, 0, 0, 0}, {3900, 0}},
    {9300, {0, 0, 0, 0, 0}, {4050, 0}},
    {9750, {0, 0, 0, 0, 0}, {9813, 0}},
    {2583, {68, 382, 60, 0, 0}, {4229, 9000}},
    {6000, {0, 0, 0, 0, 0}, {9750, 0}},
    {8550, {321, 0, 0, 0, 0}, {10508, 4573}},
    {0, {0, 0, 0, 0, 0}, {4682, 0}},
    {8550, {341, 0, 0, 0, 0}, {10508, 4573}},
    {0, {0, 0, 0, 0, 0}, {4682, 0}},
    {2598, {68, 60, 0, 0, 0}, {9033, 1213}},
    {8114, {282, 0, 0, 0, 0}, {9600, 0}},
    {1239, {0, 0, 0, 0, 0}, {1220, 0}},
    {8250, {0, 0, 0, 0, 0}, {4984, 0}},
    {162, {62, 60, 0, 0, 0}, {8170, 9600}},
    {9012, {82, 161, 221, 80, 60}, {8303, 5250}},
    {7683, {382, 0, 0, 0, 0}, {4384, 0}},
    {6646, {181, 180, 200, 0, 0}, {2609, 7800}},
    {6646, {102, 0, 0, 0, 0}, {2550, 0}},
    {2564, {262, 68, 260, 280, 60}, {8303, 5460}},
    {6150, {244, 10, 0, 0, 0}, {5550, 0}},
    {6150, {244, 11, 140, 0, 0}, {8170, 9600}},
    {9000, {166, 0, 0, 0, 0}, {5700, 0}},
    {5565, {483, 161, 160, 220, 140}, {12789, 9358}},
    {5565, {161, 247, 160, 220, 0}, {12789, 9366}},
    {1541, {122, 120, 0, 0, 0}, {7816, 0}},
    {2100, {321, 320, 340, 0, 0}, {2636, 372}},
    {2100, {341, 0, 0, 0, 0}, {2551, 9150}},
    {6150, {247, 0, 0, 0, 0}, {6000, 0}},
    {2850, {0, 0, 0, 0, 0}, {4950, 0}},
    {5565, {162, 0, 0, 0, 0}, {2485, 6150}},
    {1212, {0, 0, 0, 0, 0}, {6300, 0}},
    {1220, {0, 0, 0, 0, 0}, {6300, 0}},
    {1241, {0, 0, 0, 0, 0}, {6300, 0}},
    {2576, {68, 244, 60, 160, 140}, {9062, 2400}},
    {162, {164, 260, 0, 0, 0}, {8170, 9600}},
    {8550, {244, 0, 0, 0, 0}, {1231, 4843}},
    {8550, {164, 0, 0, 0, 0}, {1231, 4800}},
    {8550, {184, 0, 0, 0, 0}, {1244, 0}},
    {9614, {221, 262, 280, 260, 0}, {8005, 0}},
    {184, {282, 120, 0, 0, 0}, {8156, 10564}},
    {1537, {42, 0, 0, 0, 0}, {6811, 0}},
    {1507, {0, 0, 0, 0, 0}, {1246, 0}},
    {7050, {401, 305, 0, 0, 0}, {7050, 0}},
    {2100, {0, 0, 0, 0, 0}, {4950, 0}},
    {8550, {24, 0, 0, 0, 0}, {1231, 4800}},
    {939, {166, 0, 0, 0, 0}, {5700, 0}},
    {5565, {0, 0, 0, 0, 0}, {2485, 4950}},
    {175, {84, 100, 0, 0, 0}, {8170, 9600}},
    {8550, {0, 0, 0, 0, 0}, {4998, 0}},
    {9000, {0, 0, 0, 0, 0}, {4984, 0}},
    {900, {0, 0, 0, 0, 0}, {7350, 0}},
    {2550, {68, 60, 0, 0, 0}, {9028, 4950}},
    {9945, {0, 0, 0, 0, 0}, {2471, 0}},
    {8700, {0, 0, 0, 0, 0}, {4950, 0}},
    {5550, {0, 0, 0, 0, 0}, {2485, 4950}},
    {6600, {0, 0, 0, 0, 0}, {7500, 0}},
    {1200, {0, 0, 0, 0, 0}, {3000, 0}},
    {9471, {603, 0, 0, 0, 0}, {10586, 7723}},
    {0, {0, 0, 0, 0, 0}, {15403, 15673}},
    {0, {0, 0, 0, 0, 0}, {15856, 0}},
    {5125, {0, 0, 0, 0, 0}, {2250, 0}},
    {8400, {0, 0, 0, 0, 0}, {6000, 0}},
    {7697, {0, 0, 0, 0, 0}, {3000, 0}},
    {7650, {0, 0, 0, 0, 0}, {3900, 0}},
    {5100, {0, 0, 0, 0, 0}, {2507, 11400}},
    {2550, {0, 0, 0, 0, 0}, {16200, 0}},
    {175, {284, 300, 0, 0, 0}, {8176, 0}},
};
char* Messages[] = {
    "[dummy]",
    "My bites have rotted my whole body!",
    "My chigger bites are now INFECTED!",
    "You lost *ALL* treasures.",
    "The mud dried up and fell off.",
    "I'm bitten by chiggers.",
    "Too dry, the fish died.",
    "Dragon smells something. Awakens & attacks me!",
    "A voice BOOOOMS out:",
    "Welcome to Adventure International's Mini-Adventure Sampler!",
    "This is a small but complete Adventure. You must find the 3",
    "hidden Treasures and store them away! Say: 'score' to see",
    "how well you're doing!",
    "Remember you can always say 'HELP'",
    "Maybe I should read it?",
    "There's something there all right! Maybe I should go there?",
    "OK",
    "BOY that really hit the spot!",
    "nothing to light it with",
    "gas needs to be contained before it will burn",
    "I don't know where it is",
    "First I need an empty container.",
    "Chop 'er down!",
    "TIMBER. Something fell from the tree top & vanished in the swamp",
    "I can't its locked",
    "In 2 words tell me at what...like: AT TREE",
    "How?",
    "To stop game say QUIT",
    "OK, I threw it.",
    "It doesn't seem to bother him at all!",
    "Medicine is good for bites.",
    "Try --> 'LOOK, JUMP, SWIM, CLIMB, FIND, TAKE, SCORE, DROP'",
    "and any other verbs you can think of...",
    "Nothing happens",
    "Maybe if I threw something?...",
    "TIMBER!",
    "Lock shatters",
    "Somethings too heavy. I fall.",
    "I'm not carrying ax, take inventory!",
    "Something I'm holding vibrates and...",
    "Not here.",
    "The ax vibrated!",
    "Try the swamp",
    "You may need to say magic words here",
    "There are only 3 ways to wake the Dragon!",
    "I'm bit by a spider",
    "please leave it alone",
    "That won't ignite",
    "You might try examining things...",
    "Sorry, I can only throw the ax.",
    "Huh? I don't think so!",
    "This Mini-Adventure is but a small sample of what awaits you in",
    "our Classic: ADVENTURELAND, which contains an additional 10",
    "treasures, magic carpets, killer bees, burning lava and much",
    "much more!",
    "Purchase a copy of 'ADVENTURELAND' from your favorite dealer",
    "today!",
    "I see nothing special",
    "What?",
    "All treasures collected.",
};
const GameState InitState = {
	0, 0, PLAYER_START, LIGHT_REFILL, 0, 0, 0, {0}, {0},
	{
		4,
		4,
		2,
		0,
		1,
		7,
		1,
		7,
		7,
		3,
		0,
		2,
		3,
		5,
		0,
		1,
		0,
		0,
		1,
		9,
		15,
		9,
		1,
		11,
		12,
		17,
		7,
		0,
		11,
		8,
		14,
		14,
	}
};
#else
extern Item Items[];
extern char* Verbs[];
extern char* Nouns[];
extern Room Rooms[];
extern Action Actions[];
extern char* Messages[];
extern const GameState InitState;
#endif
